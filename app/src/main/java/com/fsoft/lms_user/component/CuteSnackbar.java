package com.fsoft.lms_user.component;

import android.content.Context;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.annotation.StringRes;

import com.fsoft.lms_user.R;
import com.google.android.material.color.MaterialColors;
import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;

// https://www.journaldev.com/10324/android-snackbar-example-tutorial
public class CuteSnackbar {
    private final Snackbar snackbar;
    private final TextView textView;

    public CuteSnackbar (Context context, @NonNull View view) {
        snackbar =
                Snackbar.make(view, R.string.placeholder_action, Snackbar.LENGTH_LONG)
                        .setBackgroundTint(MaterialColors.getColor(view, R.attr.colorOnPrimary,
                                context.getColor(R.color.white)))
                        .setTextColor(MaterialColors.getColor(view, R.attr.colorSecondaryVariant,
                                context.getColor(R.color.gray)));
        textView = snackbar.getView().findViewById(com.google.android.material.R.id.snackbar_text);
        textView.setGravity(Gravity.CENTER_VERTICAL);
        textView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_launcher_foreground, 0, 0, 0);
        textView.setCompoundDrawablePadding(context.getResources().getDimensionPixelOffset(R.dimen.buttonIcon_paddingStart));
    }

    public CuteSnackbar (Context context, @NonNull View view, @BaseTransientBottomBar.Duration int duration) {
        this(context, view);
        snackbar.setDuration(duration);
    }

    public TextView getTextView() {
        return textView;
    }

    public CuteSnackbar setContent(@DrawableRes int drawable, @StringRes int strId) {
        textView.setCompoundDrawablesWithIntrinsicBounds(drawable, 0, 0, 0);
        snackbar.setText(strId);
        return this;
    }

    public CuteSnackbar setAction(@StringRes int strId, View.OnClickListener listener) {
        snackbar.setAction(strId, listener);
        return this;
    }

    public void show() {
        snackbar.show();
    }
    
    public void dismiss() {
        snackbar.dismiss();
    }
}