package com.fsoft.lms_user.fragment;

import android.os.Bundle;

import androidx.annotation.StringRes;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.fragment.app.Fragment;

import android.text.Editable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fsoft.lms_user.R;
import com.fsoft.lms_user.abtract.TextChangedListener;
import com.fsoft.lms_user.activity.AddQuestionActivity;
import com.fsoft.lms_user.callback.FragmentCallbacks;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.util.Objects;

@SuppressWarnings("FieldCanBeLocal")
public class OpenEndedFragment extends Fragment implements FragmentCallbacks {
    private TextInputLayout textInputAnswer;
    private TextInputEditText editTextAnswer;

    private final String TAG = "OpenEndedFragment";

    public OpenEndedFragment() {
        // Required empty public constructor
    }

    public static OpenEndedFragment newInstance() {
        OpenEndedFragment fragment = new OpenEndedFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_open_ended, container, false);

        // Set up UI
        textInputAnswer = view.findViewById(R.id.textInputAnswer);
        editTextAnswer = view.findViewById(R.id.editTextAnswer);
        editTextAnswer.setOnFocusChangeListener((v, focused)
                -> onFocusChange(textInputAnswer, focused, R.string.hint_open_ended));
        editTextAnswer.addTextChangedListener(
                new TextChangedListener<TextInputEditText>(editTextAnswer) {
                    @Override
                    public void onTextChanged(TextInputEditText target, Editable s) {
                        setAnswer(s);
                    }
                });

        return view;
    }

    private void onFocusChange(TextInputLayout inputLayout, boolean focused, @StringRes int stringId) {
        if (!focused) {
            String str = Objects.requireNonNull(inputLayout.getEditText()).getText().toString();
            if (str.isEmpty()) {
                inputLayout.setHint(stringId);
                inputLayout.setBackground(
                        AppCompatResources.getDrawable(
                                requireContext(),
                                R.drawable.rounded_input_negative
                        )
                );
            } else {
                inputLayout.setHint("");
                inputLayout.setBackground(
                        AppCompatResources.getDrawable(
                                requireContext(),
                                R.drawable.rounded_input
                        )
                );
            }
        }
    }

    private void setAnswer(Editable s) {
        updateQuestion("answer", "[\"" + s + "\"]");
        updateQuestion("correct_answer", "[\"" + s + "\"]");
    }

    private void updateQuestion(String key, String val) {
        Bundle bundle = new Bundle();
        bundle.putString("key", key);
        bundle.putString("val", val);
        ((AddQuestionActivity) requireContext()).fragToMain("checkbox", bundle);
    }

    @Override
    public void mainToFrag(Bundle bundle) {
        if (bundle.containsKey("action")) {
            if ("checkAnswers".equals(bundle.getString("action"))) {
                onFocusChange(textInputAnswer, false, R.string.hint_open_ended);
            } else {
                Log.e(TAG, "fragToMain received unknown action: " + bundle.getString("action"));
            }
        } else {
            Log.e(TAG, "mainToFrag received invalid bundle: " + bundle);
        }
    }
}