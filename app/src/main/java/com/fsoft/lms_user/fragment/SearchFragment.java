package com.fsoft.lms_user.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.fsoft.lms_user.APIService;
import com.fsoft.lms_user.R;
import com.fsoft.lms_user.activity.CreateQuizActivity;
import com.fsoft.lms_user.activity.ViewQuizDetailActivity;
import com.fsoft.lms_user.adapter.QuizAdapter;
import com.fsoft.lms_user.model.Quiz;
import com.fsoft.lms_user.model.Search;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.fsoft.lms_user.activity.MainActivity.loggedInUser;

public class SearchFragment extends Fragment {
    private ProgressDialog progressDialog;

    private SearchView searchViewSearchQuiz;
    private TextView tvSorryInSearchQuiz;

    private RecyclerView recyclerViewQuizResultsList;
    private QuizAdapter quizAdapter;
    private RecyclerView.LayoutManager layoutManager;

    ArrayList<Quiz> quizzesList;
    String username, jwtToken;

    //-------------------------------------------------------------------------------
    HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY);

    OkHttpClient okHttpClient = new OkHttpClient.Builder()
            .addInterceptor(loggingInterceptor)
            .build();

    Retrofit retrofit = new Retrofit.Builder()
            .baseUrl("https://backend-lms.herokuapp.com/")
            .addConverterFactory(GsonConverterFactory.create())
            .client(okHttpClient)
            .build();

    APIService apiService = retrofit.create(APIService.class);
    //-------------------------------------------------------------------------------
    public SearchFragment() {
        // Required empty public constructor
    }

    public static SearchFragment newInstance() {
        return new SearchFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (TextUtils.isEmpty(loggedInUser)) {
            return inflater.inflate(R.layout.fragment_need_login, container, false);
        }
        View view = inflater.inflate(R.layout.fragment_search, container, false);

        //Initialize all widgets ids
        searchViewSearchQuiz = view.findViewById(R.id.searchViewSearchQuiz);
        recyclerViewQuizResultsList = view.findViewById(R.id.recyclerViewSearchQuizResultsList);
        tvSorryInSearchQuiz = view.findViewById(R.id.tvSorryInSearchQuiz);

        //Set up search view to search quiz
        searchViewSearchQuiz.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                quizzesList.clear();
                quizAdapter.notifyDataSetChanged();
                searchQuiz(query);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        //Set up recyclerview
        quizzesList = new ArrayList<>();
        layoutManager = new LinearLayoutManager(getActivity());
        quizAdapter = new QuizAdapter(quizzesList);

        recyclerViewQuizResultsList.setHasFixedSize(true);
        recyclerViewQuizResultsList.setLayoutManager(layoutManager);
        recyclerViewQuizResultsList.setAdapter(quizAdapter);

        quizAdapter.setOnItemClickListener(new QuizAdapter.OnItemCLickListener() {
            @Override
            public void onItemClick(int position) {
                Quiz clickedQuiz = quizzesList.get(position);

                Intent intent = new Intent(getActivity(), ViewQuizDetailActivity.class);
                intent.putExtra("quiz", clickedQuiz);
                startActivity(intent);
            }
        });

        //Get JWT Token to access APIs
        getJWTToken();

        //Set up loading dialog
        progressDialog = new ProgressDialog(getActivity());

        return  view;
    }

    //-------------------------------------------------------------------------------
    private void showProgressDialog(String text) {
        progressDialog.setTitle(text);
        progressDialog.show();
    }

    //-------------------------------------------------------------------------------
    private void getJWTToken() {
        SharedPreferences loginPref = getActivity().getSharedPreferences("Login", Context.MODE_PRIVATE);
        username = loginPref.getString("loggedInUser", null);
        jwtToken = loginPref.getString("accessToken", null);
    }
    //-------------------------------------------------------------------------------
    private void searchQuiz(String quizName){
        showProgressDialog("Searching..");

        Search search = new Search(quizName);
        //Call API to get all search results
        Call<List<Quiz>> call = apiService.searchQuizByName(jwtToken, search);

        call.enqueue(new Callback<List<Quiz>>() {
            @Override
            public void onResponse(Call<List<Quiz>> call, Response<List<Quiz>> response) {
                if (progressDialog.isShowing()){
                    progressDialog.dismiss();
                }

                if (!response.isSuccessful()){
                    Toast.makeText(getActivity(), "Search quiz failed", Toast.LENGTH_SHORT).show();
                    System.out.println("code: " + response.code());
                    return;
                }

                Toast.makeText(getActivity(), "Search quiz successfully", Toast.LENGTH_SHORT).show();

                List<Quiz> responseQuizzes = response.body();
                quizzesList.addAll(responseQuizzes);

                quizAdapter.notifyDataSetChanged();

                if (quizzesList.size() < 1){
                    tvSorryInSearchQuiz.setVisibility(View.VISIBLE);
                } else {
                    tvSorryInSearchQuiz.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void onFailure(Call<List<Quiz>> call, Throwable t) {
                System.out.println("Retrofit onFailure: " + t.getMessage());
                if (progressDialog.isShowing()){
                    progressDialog.dismiss();
                }

                Toast.makeText(getActivity(), "No response received from API", Toast.LENGTH_SHORT).show();
            }
        });

    }

}