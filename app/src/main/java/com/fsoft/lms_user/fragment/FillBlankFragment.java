package com.fsoft.lms_user.fragment;

import android.os.Bundle;

import androidx.annotation.StringRes;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.fragment.app.Fragment;

import android.text.Editable;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.fsoft.lms_user.R;
import com.fsoft.lms_user.abtract.TextChangedListener;
import com.fsoft.lms_user.activity.AddQuestionActivity;
import com.fsoft.lms_user.callback.FragmentCallbacks;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Objects;

@SuppressWarnings("FieldCanBeLocal")
public class FillBlankFragment extends Fragment implements AdapterView.OnItemSelectedListener, FragmentCallbacks {
    // GUI elements
    private Spinner spinnerFillBlank;
    private TextInputLayout textInputAnswer;
    private TextInputEditText editTextAnswer;
    private Spinner spinnerFillBlankAlt;
    private TextInputLayout textInputAnswerAlt;
    private TextInputEditText editTextAnswerAlt;
    private final ArrayList<String> answersOptions = new ArrayList<>(Arrays.asList("", ""));

    private final String TAG = "FillBlankFragment";

    // Data
    private final String[] questionTypes = {"is exactly", "contains"};

    public FillBlankFragment() {
        // Required empty public constructor
    }

    public static FillBlankFragment newInstance() {
        FillBlankFragment fragment = new FillBlankFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_fill_blank, container, false);

        // Set up spinners
        spinnerFillBlank = view.findViewById(R.id.spinnerAnswer);
        ArrayAdapter<String> adapter =
                new ArrayAdapter<>(requireContext(), android.R.layout.simple_spinner_dropdown_item, questionTypes);
        spinnerFillBlank.setAdapter(adapter);
        spinnerFillBlank.setOnItemSelectedListener(this);
        spinnerFillBlankAlt = view.findViewById(R.id.spinnerAnswerAlt);
        spinnerFillBlankAlt.setAdapter(adapter);
        spinnerFillBlankAlt.setOnItemSelectedListener(this);

        // Set up inputs
        spinnerFillBlank = view.findViewById(R.id.spinnerAnswer);
        textInputAnswer = view.findViewById(R.id.textInputAnswer);
        editTextAnswer = view.findViewById(R.id.editTextAnswer);
        editTextAnswer.setOnFocusChangeListener((v, focused)
                -> onFocusChange(textInputAnswer, focused, R.string.hint_fill_blank));
        editTextAnswer.addTextChangedListener(
                new TextChangedListener<TextInputEditText>(editTextAnswer) {
                    @Override
                    public void onTextChanged(TextInputEditText target, Editable s) {
                        setAnswer(0, spinnerFillBlank.getSelectedItemId(), s);
                    }
                });
        spinnerFillBlankAlt = view.findViewById(R.id.spinnerAnswerAlt);
        textInputAnswerAlt = view.findViewById(R.id.textInputAnswerAlt);
        editTextAnswerAlt = view.findViewById(R.id.editTextAnswerAlt);
        editTextAnswerAlt.addTextChangedListener(
                new TextChangedListener<TextInputEditText>(editTextAnswerAlt) {
                    @Override
                    public void onTextChanged(TextInputEditText target, Editable s) {
                        setAnswer(1, spinnerFillBlankAlt.getSelectedItemId(), s);
                    }
                });

        return view;
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {}

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {}

    private void onFocusChange(TextInputLayout inputLayout, boolean focused, @StringRes int stringId) {
        if (!focused) {
            String str = Objects.requireNonNull(inputLayout.getEditText()).getText().toString();
            if (str.isEmpty()) {
                inputLayout.setHint(stringId);
                inputLayout.setBackground(
                        AppCompatResources.getDrawable(
                                requireContext(),
                                R.drawable.rounded_input_negative
                        )
                );
            } else {
                inputLayout.setHint("");
                inputLayout.setBackground(
                        AppCompatResources.getDrawable(
                                requireContext(),
                                R.drawable.rounded_input
                        )
                );
            }
        }
    }

    private void setAnswer(int answerId, long matchId, Editable s) {
        if (s.length() > 0) {
            answersOptions.set(answerId, (matchId == 0) ? "=" + s : s.toString());
        } else {
            answersOptions.set(answerId, "");
        }
        updateQuestion("answer", getAnswerJson());
        updateQuestion("correct_answer", getAnswerJson());
    }

    private String getAnswerJson() {
        if (TextUtils.isEmpty(answersOptions.get(1))) {
            return "[\"" + answersOptions.get(0) + "\"]";
        } else {
            return "[" +
                        "\"" + answersOptions.get(0) + "\"," +
                        "\"" + answersOptions.get(1) + "\"" +
                    "]";
        }
    }

    private void updateQuestion(String key, String val) {
        Bundle bundle = new Bundle();
        bundle.putString("key", key);
        bundle.putString("val", val);
        ((AddQuestionActivity) requireContext()).fragToMain("fill-in-the-blank", bundle);
    }

    @Override
    public void mainToFrag(Bundle bundle) {
        if (bundle.containsKey("action")) {
            if ("checkAnswers".equals(bundle.getString("action"))) {
                onFocusChange(textInputAnswer, false, R.string.hint_open_ended);
            } else {
                Log.e(TAG, "fragToMain received unknown action: " + bundle.getString("action"));
            }
        } else {
            Log.e(TAG, "mainToFrag received invalid bundle: " + bundle);
        }
    }
}