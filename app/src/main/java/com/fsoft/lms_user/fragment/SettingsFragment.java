package com.fsoft.lms_user.fragment;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.fsoft.lms_user.APIService;
import com.fsoft.lms_user.R;

import com.fsoft.lms_user.model.Password;
import com.fsoft.lms_user.model.User;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.fsoft.lms_user.activity.MainActivity.loggedInUser;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class SettingsFragment extends Fragment {
    //-----------------------------------------------------
    private final static int PICK_IMAGE_REQUEST = 1;
    private StorageReference storageReference;

    RelativeLayout btnChangeFullName;
    RelativeLayout btnChangeUsername;
    RelativeLayout btnChangeDob;
    RelativeLayout btnUpdatePassword;

    EditText edtFullNameInSettings;
    EditText edtUsernameInSettings;
    TextView tvDateOfBirthInSettings;
    EditText edtCurrentPasswordInSettings;
    EditText edtNewPasswordInSettings;
    EditText edtRetypeNewPasswordInSettings;

    ImageView ivAvatar;
    Uri imageUri;

    Button btnSaveChangesInSettings;
    Button btnUpdatePasswordInSettings;

    String username, jwtToken, avatarURL;
    User user;
    ProgressDialog progressDialog;

    //---------------------Declare needed things for retrofit-----------------------------
    HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY);

    OkHttpClient okHttpClient = new OkHttpClient.Builder()
            .addInterceptor(loggingInterceptor)
            .build();

    Retrofit retrofit = new Retrofit.Builder()
            .baseUrl("https://backend-lms.herokuapp.com/")
            .addConverterFactory(GsonConverterFactory.create())
            .client(okHttpClient)
            .build();

    APIService apiService = retrofit.create(APIService.class);
    //-----------------------------------------------------
    public SettingsFragment() {
        // Required empty public constructor
    }

    public static SettingsFragment newInstance() {
        return new SettingsFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (TextUtils.isEmpty(loggedInUser)) {
            return inflater.inflate(R.layout.fragment_need_login, container, false);
        }
        View view = inflater.inflate(R.layout.fragment_settings, container, false);

        //Initialize all widgets ids
        ivAvatar = view.findViewById(R.id.ivAvatar);
        edtFullNameInSettings = view.findViewById(R.id.edtFullNameInSettings);
        edtUsernameInSettings = view.findViewById(R.id.edtUsernameInSettings);
        tvDateOfBirthInSettings = view.findViewById(R.id.tvDateOfBirthInSettings);
        btnSaveChangesInSettings = view.findViewById(R.id.btnSaveChangesInSettings);

        edtCurrentPasswordInSettings = view.findViewById(R.id.edtCurrentPasswordInSettings);
        edtNewPasswordInSettings = view.findViewById(R.id.edtNewPasswordInSettings);
        edtRetypeNewPasswordInSettings = view.findViewById(R.id.edtRetypeNewPasswordInSettings);
        btnUpdatePasswordInSettings = view.findViewById(R.id.btnUpdatePasswordInSettings);

        //Set behaviour for image view Avatar
        ivAvatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectThenUploadImage();
            }
        });
        //Set behaviour for text view Date of birth
        tvDateOfBirthInSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createDatePickerDialog();
            }
        });

        //Set behaviour for buttons Save profile and Update password
        btnSaveChangesInSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                patchUser();
                int a = 1;
            }
        });

        btnUpdatePasswordInSettings.setOnClickListener((new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String curPass = edtCurrentPasswordInSettings.getText().toString();
                String newPass = edtNewPasswordInSettings.getText().toString();
                String retypePass = edtRetypeNewPasswordInSettings.getText().toString();

                if (curPass.length() < 1 || newPass.length() < 1 || retypePass.length() < 1){
                    Toast.makeText(getActivity(), "Password length must be at least 6 characters", Toast.LENGTH_LONG).show();
                } else {
                    if (newPass.equals(retypePass)){
                        Password password = new Password(username, curPass, newPass);
                        patchPassword(password);
                    }
                    else {
                        Toast.makeText(getActivity(), "New password and Retype do not match", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        }));


        //Initialize progress dialog, JWT token and User profile
        progressDialog = new ProgressDialog(getActivity());
        getJWTToken();
        getUser();

        return view;
    }

    //-----------------------------------------------------
    private void showProgressDialog(String text) {
        progressDialog.setTitle(text);
        progressDialog.show();
    }

    private void createDatePickerDialog(){
        //Dong nay cop cua Hung, doc khong hieu cho listener, lam theo Coding in flow bi loi
        Calendar calendar = Calendar.getInstance();
        new DatePickerDialog(
                getActivity(),
                (datePicker, y, m, d) -> {
                    calendar.set(Calendar.YEAR, y);
                    calendar.set(Calendar.MONTH, m);
                    calendar.set(Calendar.DAY_OF_MONTH, d);
                    SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy", Locale.ROOT);
                    tvDateOfBirthInSettings.setText(format.format(calendar.getTime()));
                },
                calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH)).show();
    }

    //-----------------------------------------------------
    private void getJWTToken() {
        SharedPreferences loginPref = getActivity().getSharedPreferences("Login", Context.MODE_PRIVATE);
        username = loginPref.getString("loggedInUser", null);
        jwtToken = loginPref.getString("accessToken", null);
    }

    private void getUser() {
        showProgressDialog("Loading data..");

        //Call API to get user information
        Call<User> call = apiService.getUser(jwtToken, username);

        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                if (!response.isSuccessful()) {
                    Toast.makeText(getActivity(), "Load data failed", Toast.LENGTH_SHORT).show();
                    System.out.println("Code: " + response.code());
                    return;
                }

                user = response.body();
                edtFullNameInSettings.setText(user.getName());
                edtUsernameInSettings.setText(user.getUsername());
                tvDateOfBirthInSettings.setText(user.getBirthyear());

                //Load avatar from URL
                Glide.with(getContext())
                        .load(user.getAvatar())
                        .circleCrop()
                        .into(ivAvatar);

                System.out.println(user.getAvatar());
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                Toast.makeText(getActivity(), "No response received from API", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void patchUser() {
        user.setAvatar(avatarURL);
        user.setName(edtFullNameInSettings.getText().toString());
        user.setUsername(edtUsernameInSettings.getText().toString());
        user.setBirthyear(tvDateOfBirthInSettings.getText().toString());

        showProgressDialog("Saving changes..");

        //Call API to set up user information
        Call<User> call = apiService.patchUser(jwtToken, username, user);

        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                if (!response.isSuccessful()) {
                    Toast.makeText(getActivity(), "Save changes failed", Toast.LENGTH_SHORT).show();
                    System.out.println("Code: " + response.code());
                    return;
                }

                Toast.makeText(getActivity(), "Save changes successfully", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                Toast.makeText(getActivity(), "No response received from API", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void patchPassword(Password password){
        showProgressDialog("Updating password..");

        //Call API to set up user information
        Call<Password> call = apiService.patchPassword(jwtToken, password);

        call.enqueue(new Callback<Password>() {
            @Override
            public void onResponse(Call<Password> call, Response<Password> response) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                if (!response.isSuccessful()) {
                    if (response.code() == 400)
                        Toast.makeText(getActivity(), "Wrong current password", Toast.LENGTH_SHORT).show();
                    System.out.println("Code: " + response.code());
                    return;
                }

                Toast.makeText(getActivity(), "Update password successfully", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<Password> call, Throwable t) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                Toast.makeText(getActivity(), "No response received from API", Toast.LENGTH_SHORT).show();
            }
        });
    }
    //-----------------------------------------------------
    private void selectThenUploadImage() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, PICK_IMAGE_REQUEST);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && data != null && data.getData() != null) {
            imageUri = data.getData();

            Glide.with(getActivity())
                    .load(imageUri)
                    .circleCrop()
                    .into(ivAvatar);

            uploadImage();
        }
    }

    private void uploadImage() {
        showProgressDialog("Uploading avatar..");

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss", Locale.CHINA);
        Date now = new Date();
        String fileName = username + "_" + simpleDateFormat.format(now);

        // Upload selected cover image to firebase storage
        storageReference = FirebaseStorage.getInstance().getReference("images/" + fileName);
        storageReference.putFile(imageUri)
                .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        if (progressDialog.isShowing()) {
                            progressDialog.dismiss();
                        }
                        Toast.makeText(getActivity(), "Upload avatar successfully", Toast.LENGTH_SHORT).show();

                        // Get image url after upload image to firebase storage
                        storageReference.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                            @Override
                            public void onSuccess(Uri uri) {
                                avatarURL = uri.toString();
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                System.out.println("Failed to get avatar URL");
                            }
                        });
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        if (progressDialog.isShowing()) {
                            progressDialog.dismiss();
                        }
                        Toast.makeText(getActivity(), "Upload avatar failed", Toast.LENGTH_SHORT).show();
                    }
                });
    }
}