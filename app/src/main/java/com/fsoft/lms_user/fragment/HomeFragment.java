package com.fsoft.lms_user.fragment;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.appcompat.view.menu.MenuPopupHelper;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.fsoft.lms_user.R;
import com.fsoft.lms_user.activity.LoginActivity;
import com.fsoft.lms_user.adapter.RecommendationAdapter;
import com.fsoft.lms_user.callback.FragmentCallbacks;
import com.fsoft.lms_user.component.CuteSnackbar;
import com.fsoft.lms_user.model.Quiz;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

import static com.fsoft.lms_user.activity.MainActivity.API_URL;
import static com.fsoft.lms_user.activity.MainActivity.LOGIN_URL;
import static com.fsoft.lms_user.activity.MainActivity.QUIZ_URL;
import static com.fsoft.lms_user.activity.MainActivity.accessToken;
import static com.fsoft.lms_user.activity.MainActivity.loginPref;

@SuppressWarnings("FieldCanBeLocal")
public class HomeFragment extends Fragment implements FragmentCallbacks {
    private LinearLayout linearLayout;
    private ImageView info_streak_start, info_streak_start_line,
            info_streak_next, info_streak_next_line,
            info_streak_neutral, info_streak_neutral_line,
            info_streak_almost, info_streak_almost_line,
            info_streak_full;
    private TextView info_streak_count, info_streak_msg, quizCount;
    private RecyclerView recyclerView;
    private RecommendationAdapter adapter;

    private RequestQueue requestQueue;
    private ArrayList<String> avatars;
    private ArrayList<String> names;
    private ArrayList<String> authors;
    private ArrayList<Integer> qCounts;

    private int streakCount = 2;

    private final String TAG = "HomeFragment";

    public HomeFragment() {
        // Required empty public constructor
    }

    public static HomeFragment newInstance() {
        return new HomeFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Fix invalid streakCount
        if (streakCount < 0) {
            streakCount = 0;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        linearLayout = view.findViewById(R.id.linearLayout);

        // // Set up streak card
        // // Streak icons
        // info_streak_start = view.findViewById(R.id.info_streak_start);
        // info_streak_start_line = view.findViewById(R.id.info_streak_start_line);
        // info_streak_next = view.findViewById(R.id.info_streak_next);
        // info_streak_next_line = view.findViewById(R.id.info_streak_next_line);
        // info_streak_neutral = view.findViewById(R.id.info_streak_neutral);
        // info_streak_neutral_line = view.findViewById(R.id.info_streak_neutral_line);
        // info_streak_almost = view.findViewById(R.id.info_streak_almost);
        // info_streak_almost_line = view.findViewById(R.id.info_streak_almost_line);
        // info_streak_full = view.findViewById(R.id.info_streak_full);
        // switch (Math.min(streakCount, 5)) {
        //     case 5:
        //         info_streak_full.setAlpha(1f);
        //         info_streak_almost_line.setAlpha(1f);
        //         info_streak_full.getDrawable().setTintList(null);
        //         info_streak_almost_line.getDrawable().setTintList(null);
        //     case 4:
        //         info_streak_almost.setAlpha(1f);
        //         info_streak_neutral_line.setAlpha(1f);
        //         info_streak_almost.getDrawable().setTintList(null);
        //         info_streak_neutral_line.getDrawable().setTintList(null);
        //     case 3:
        //         info_streak_neutral.setAlpha(1f);
        //         info_streak_next_line.setAlpha(1f);
        //         info_streak_neutral.getDrawable().setTintList(null);
        //         info_streak_next_line.getDrawable().setTintList(null);
        //     case 2:
        //         info_streak_next.setAlpha(1f);
        //         info_streak_start_line.setAlpha(1f);
        //         info_streak_next.getDrawable().setTintList(null);
        //         info_streak_start_line.getDrawable().setTintList(null);
        //     case 1:
        //         info_streak_start.setAlpha(1f);
        //         info_streak_start.getDrawable().setTintList(null);
        // }
        // // Streak text
        // info_streak_count = view.findViewById(R.id.info_streak_count);
        // info_streak_count.setText(getString(R.string.info_streak_count, streakCount));
        // info_streak_msg = view.findViewById(R.id.info_streak_msg);
        // if (streakCount == 0) {
        //     info_streak_msg.setText(getString(R.string.info_streak_msg_none));
        // } else if (streakCount >= 1 && streakCount < 3) {
        //     info_streak_msg.setText(getString(R.string.info_streak_msg_start));
        // } else if (streakCount >= 3 && streakCount < 5) {
        //     info_streak_msg.setText(getString(R.string.info_streak_msg_neutral));
        // } else if (streakCount >= 5) {
        //     info_streak_msg.setText(getString(R.string.info_streak_msg_full));
        // }

        // Set up recommendations
        avatars = new ArrayList<>();
        names = new ArrayList<>();
        authors = new ArrayList<>();
        qCounts = new ArrayList<>();
        requestQueue = Volley.newRequestQueue(requireContext());
        quizCount = view.findViewById(R.id.quizCount);
        recyclerView = view.findViewById(R.id.recyclerView);
        getQuiz();

        return view;
    }

    private void getQuiz() {
        // Set up Snackbar
        CuteSnackbar snackbar = new CuteSnackbar(requireContext(), linearLayout);

        // Send GET request
        JsonArrayRequest jsonRequest = new JsonArrayRequest(
                Request.Method.GET,
                QUIZ_URL,
                null,
                response -> {
                    Log.i(TAG, "GET Quiz responded with json: " + response.toString());
                    try {
                        int count = 0;
                        ArrayList<Quiz> quizzes = new ArrayList<>();
                        for (int i = response.length() - 1; i >= 0; --i) {
                            JSONObject obj = response.getJSONObject(i);
                            if (!obj.has("avatar")) {
                                obj.put("avatar","none");
                            }
                            if (!obj.has("author")) {
                                continue;
                            }
                            count++;
                            ArrayList<String> questions = new ArrayList<>();
                            JSONArray qArray = obj.getJSONArray("question");
                            for (int j = 0 ; j < qArray.length() ; j++){
                                questions.add(qArray.getString(j));
                            }
                            quizzes.add(new Quiz(
                                    obj.getString("_id"),
                                    obj.getString("name"),
                                    obj.getString("category"),
                                    obj.getString("avatar"),
                                    obj.getString("privacy"),
                                    obj.getString("author"),
                                    obj.getString("time"),
                                    questions
                            ));
                        }

                        // Update UI
                        quizCount.setText(String.valueOf(count));
                        adapter = new RecommendationAdapter(getActivity(), quizzes);
                        recyclerView.setAdapter(adapter);
                    } catch (JSONException e) {
                        Log.e(TAG, "GET Quiz met with an error: " + e.getMessage());
                        snackbar.setContent(R.drawable.exclamation, R.string.info_quiz_get_failed)
                                .show();
                    }
                }, error -> {
            Log.e(TAG, "GET Quiz response produced error: " + error.getMessage());
            snackbar.setContent(R.drawable.exclamation, R.string.info_quiz_get_failed);
        }) {

            @Override
            public Map<String, String> getHeaders() {
                return new HashMap<String, String>() {
                    {
                        put("access-token", accessToken);
                    }
                };
            }
        };

        jsonRequest.setTag(TAG);
        requestQueue.add(jsonRequest);
    }


    @Override
    public void mainToFrag(Bundle bundle) {
        // Check action sent
        if (bundle.getString("action") != null) {
            String action = bundle.getString("action");
            if (action.equals("refresh")) {
                getQuiz();
            } else {
                Log.w(TAG, "fragToMain received unknown action request: " + action);
            }
        }
    }
}