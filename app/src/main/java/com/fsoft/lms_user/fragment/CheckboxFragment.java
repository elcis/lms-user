package com.fsoft.lms_user.fragment;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.fsoft.lms_user.R;
import com.fsoft.lms_user.adapter.CheckboxAdapter;
import com.fsoft.lms_user.adapter.MultipleChoiceAdapter;
import com.fsoft.lms_user.callback.FragmentCallbacks;

import java.util.ArrayList;
import java.util.Arrays;

@SuppressWarnings("FieldCanBeLocal")
public class CheckboxFragment extends Fragment implements FragmentCallbacks {
    private RecyclerView answerListView;
    private LinearLayout addAnswerLayout;
    private CheckboxAdapter adapter;

    private final String TAG = "CheckboxFragment";

    public CheckboxFragment() {
        // Required empty public constructor
    }

    public static CheckboxFragment newInstance() {
        CheckboxFragment fragment = new CheckboxFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_checkbox, container, false);

        // Set up UI
        answerListView = view.findViewById(R.id.answerListView);
        addAnswerLayout = view.findViewById(R.id.addAnswerLayout);

        // Set up data
        ArrayList<String> answersOptions = new ArrayList<>(Arrays.asList("", "", "", ""));
        adapter = new CheckboxAdapter(requireContext(), answersOptions, answerListView, addAnswerLayout);
        answerListView.setAdapter(adapter);
        addAnswerLayout.setOnClickListener((v) -> adapter.addAnswer());

        return view;
    }

    @Override
    public void mainToFrag(Bundle bundle) {
        if (bundle.containsKey("action")) {
            if ("checkAnswers".equals(bundle.getString("action"))) {
                adapter.checkAnswers();
            } else {
                Log.e(TAG, "fragToMain received unknown action: " + bundle.getString("action"));
            }
        } else {
            Log.e(TAG, "mainToFrag received invalid bundle: " + bundle);
        }
    }
}