package com.fsoft.lms_user.fragment;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;

import com.fsoft.lms_user.R;

import static com.fsoft.lms_user.activity.MainActivity.loggedInUser;

public class ActivityFragment extends Fragment {

    public ActivityFragment() {
        // Required empty public constructor
    }

    public static ActivityFragment newInstance() {
        return new ActivityFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (TextUtils.isEmpty(loggedInUser)) {
            return inflater.inflate(R.layout.fragment_need_login, container, false);
        }
        View view = inflater.inflate(R.layout.fragment_activity, container, false);

        return view;
    }
}