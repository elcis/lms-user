package com.fsoft.lms_user.fragment;

import android.os.Bundle;

import androidx.annotation.StringRes;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.fragment.app.Fragment;

import android.text.Editable;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.fsoft.lms_user.R;
import com.fsoft.lms_user.abtract.TextChangedListener;
import com.fsoft.lms_user.activity.AddQuestionActivity;
import com.fsoft.lms_user.callback.FragmentCallbacks;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Objects;

@SuppressWarnings("FieldCanBeLocal")
public class TrueFalseFragment extends Fragment implements FragmentCallbacks {
    // GUI elements
    private MaterialButton action_true, action_false, action_not_given;

    private final String TAG = "TrueFalseFragment";

    public TrueFalseFragment() {
        // Required empty public constructor
    }

    public static TrueFalseFragment newInstance() {
        TrueFalseFragment fragment = new TrueFalseFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_true_false, container, false);

        // Set up buttons
        action_true = view.findViewById(R.id.action_true);
        action_true.setOnClickListener(this::setAnswer);
        action_false = view.findViewById(R.id.action_false);
        action_false.setOnClickListener(this::setAnswer);
//        action_not_given = view.findViewById(R.id.action_not_given);
//        action_not_given.setOnClickListener(this::setAnswer);

        return view;
    }

    private void setAnswer(View view) {
        String a = "[\"";
        switch (view.getId()) {
            case R.id.action_true:
                a += "0";
                break;
            case R.id.action_false:
                a += "1";
                break;
//            case R.id.action_not_given:
//                a += "2";
//                break;
            default:
                Log.e(TAG, "setAnswer received unknown button: " + view.getId());
                return;
        }
        a += "\"]";
//        updateQuestion("answer", "[\"True\", \"False\", \"Not Given\"]");
        updateQuestion("answer", "[\"True\", \"False\"]");
        updateQuestion("correct-answer", a);
    }

    private void updateQuestion(String key, String val) {
        Bundle bundle = new Bundle();
        bundle.putString("key", key);
        bundle.putString("val", val);
        ((AddQuestionActivity) requireContext()).fragToMain("true-false", bundle);
    }

    @Override
    public void mainToFrag(Bundle bundle) {
        if (bundle.containsKey("action")) {
            if ("checkAnswers".equals(bundle.getString("action"))) {
                Log.i(TAG, TAG + " does not have to " + bundle.getString("action") + ".");
            } else {
                Log.e(TAG, "fragToMain received unknown action: " + bundle.getString("action"));
            }
        } else {
            Log.e(TAG, "mainToFrag received invalid bundle: " + bundle);
        }
    }
}