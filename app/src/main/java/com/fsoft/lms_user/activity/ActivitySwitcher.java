package com.fsoft.lms_user.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContract;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;

import org.jetbrains.annotations.NotNull;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Objects;

import static com.fsoft.lms_user.activity.MainActivity.FACEBOOK_REQUEST_CODE;
import static com.fsoft.lms_user.activity.MainActivity.GOOGLE_REQUEST_CODE;
import static com.fsoft.lms_user.activity.MainActivity.GSO;
import static com.fsoft.lms_user.activity.MainActivity.LOGIN_REQUEST_CODE;
import static com.fsoft.lms_user.activity.MainActivity.SIGNUP_PROFILE_REQUEST_CODE;
import static com.fsoft.lms_user.activity.MainActivity.SIGNUP_REQUEST_CODE;


public class ActivitySwitcher {
    private final Context context;
    private String caller;

    private final HashMap<Integer, ActivityResultLauncher<Intent>> launchers;
    private final ActivityResultContract<Intent, ActivityResult> activityResultContract;
    private final ActivityResultCallback<ActivityResult> activityResultCallback, activityResultCallbackGoogle;

    private static final String TAG = "ActivityManager";

    // <editor-fold desc="CONSTRUCTORS">
    public ActivitySwitcher(Context context, String caller) {
        this.context = context;
        this.caller = caller;
        launchers = new HashMap<>();

        // Determine if this context is an activity or a fragment
        if (context == null || caller == null) {
            activityResultContract = null;
            activityResultCallback = null;
            activityResultCallbackGoogle = null;
            Log.e(TAG, "ActivitySwitcher: context or caller is null");
            return;
        }

        // Create template items
        activityResultContract = new ActivityResultContracts.StartActivityForResult();
        activityResultCallback = result -> {
            Intent data = result.getData();

            // Check for which activity returned to MainActivity
            if (data != null) {
                if (data.hasExtra("caller")) {
                    switchFragmentRequest();
                    return;
                }
            }
            switchFragmentRequest();
        };
        activityResultCallbackGoogle = result -> {
            Intent data = result.getData();

            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            Bundle bundle = new Bundle();
            try {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = task.getResult(ApiException.class);
                Log.i(TAG, "firebaseAuthWithGoogle:" + account.getId());
                bundle.putString("id", account.getId());
                bundle.putString("idToken", account.getIdToken());
            } catch (ApiException e) {
                // Google Sign In failed, update UI appropriately
                Log.w(TAG, "Google sign in failed", e);
                bundle.putString("err", e.getMessage());
            }
            ((LoginActivity) context).fragToMain("google", bundle);
        };

        // Start registering
        switch (caller) {
            case "main":
                registerForMain();
                break;
            case "home":
                registerForHome();
                break;
            case "search":
                registerForSearch();
                break;
            case "activity":
                registerForActivity();
                break;
            case "settings":
                registerForSettings();
                break;
            case "login":
                registerForLogin();
                break;
            default:
                Log.w(TAG, "ActivitySwitcher: Unknown caller " + caller);
                break;
        }
    }
    // </editor-fold>

    // <editor-fold desc="REGISTER FOR CURRENT CONTEXT">
    private void registerForMain() {
        registerLogin();
    }

    private void registerForHome() {
        // Any activity that can be launched from HomeFragment is registered here
    }

    private void registerForSearch() {
        // Any activity that can be launched from SearchFragment is registered here
    }

    private void registerForActivity() {
        // Any activity that can be launched from ActivityFragment is registered here
    }

    private void registerForSettings() {
        // Any activity that can be launched from SettingsFragment is registered here
    }

    private void registerForLogin() {
        registerLoginGoogle();
    }
    // </editor-fold>

    // <editor-fold desc="REGISTER ACTIVITIES">
    private void registerLogin() {
        launchers.put(LOGIN_REQUEST_CODE,
                ((AppCompatActivity) context).registerForActivityResult(
                        activityResultContract,
                        activityResultCallback
                )
        );
    }

    private void registerLoginGoogle() {
        launchers.put(GOOGLE_REQUEST_CODE,
                ((AppCompatActivity) context).registerForActivityResult(
                        activityResultContract,
                        activityResultCallbackGoogle
                )
        );
    }
    // </editor-fold>

    // <editor-fold desc="SHOW ACTIVITY GROUP">
    public void backToMain(String caller) {
        this.caller = caller;
        Intent intent = new Intent(context, MainActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString("caller", caller);
        intent.putExtras(bundle);
        intent.setFlags(0);
        context.startActivity(intent);
    }

    public void backToMain() {
        backToMain("home");
    }

    public void startLogin(String caller) {
        this.caller = caller;
        Intent intent = new Intent(context, LoginActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString("caller", caller);
        intent.putExtras(bundle);
        intent.setFlags(0);
        startActivity(intent, LOGIN_REQUEST_CODE);
    }

    public void startLoginGoogle() {
        Intent intent = GoogleSignIn.getClient(context, GSO).getSignInIntent();
        startActivity(intent, GOOGLE_REQUEST_CODE);
    }
    // </editor-fold>

    // <editor-fold desc="RESULT GROUP"
    public void setResult(String caller, Bundle bundle) {
        // Check for context
        if (context == null) {
            Log.w(TAG, "setResult: context is null");
            return;
        }

        // Check for caller and create new Intent
        if (TextUtils.isEmpty(caller)) {
            caller = "home";
        }
        Intent intent = new Intent();
        bundle.putString("caller", caller);
        intent.putExtras(bundle);

        // Put Intent into context's result
        ((AppCompatActivity) context).setResult(Activity.RESULT_OK, intent);
    }

    public void setResult(String caller) {
        setResult(caller, new Bundle());
    }
    // </editor-fold>

    // <editor-fold desc="PRIVATE HANDLE GROUP">
    private void startActivity(Intent intent, int requestCode) {
        try {
            if (intent != null) {
                Objects.requireNonNull(launchers.get(requestCode)).launch(intent);
            }
        } catch (Throwable e) {
            Log.e(TAG, "startActivity failed with message: " + e.getMessage());
        }
    }

    private void switchFragmentRequest() {
        try {
            if (Arrays.asList("home", "search", "activity", "settings").contains(caller)) {
                Bundle bundle = new Bundle();
                bundle.putString("caller", caller);
                bundle.putString("action", "switch");
                ((MainActivity) context).fragToMain(caller, bundle);
            }
        } catch (Exception ignored) {}
    }
    // </editor-fold>
}