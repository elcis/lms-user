package com.fsoft.lms_user.activity;

import android.animation.ObjectAnimator;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.Interpolator;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.content.res.AppCompatResources;

import com.fsoft.lms_user.R;

public class SplashActivity extends AppCompatActivity {
    private final static int SPLASH_DURATION = 4000;

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_splash);

        // Blend statusbar into background
        // REF: https://stackoverflow.com/a/43405486
        Window window = getWindow();
        Drawable background = AppCompatResources.getDrawable(this, R.drawable.ic_launcher_background);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(getColor(android.R.color.transparent));
        window.setNavigationBarColor(getColor(android.R.color.transparent));
        window.setBackgroundDrawable(background);

        // App icon animation
        ImageView app_icon = findViewById(R.id.app_icon);
        TextView app_name = findViewById(R.id.app_name);
        Animation anim_alpha = new AlphaAnimation(0f, 1f),
                anim_bounce = AnimationUtils.loadAnimation(this, R.anim.bounce);
        anim_bounce.setRepeatMode(Animation.REVERSE);
        anim_bounce.setStartOffset(500);
        anim_bounce.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {}

            @Override
            public void onAnimationEnd(Animation animation) {
                // Start app name animation after this animation
                app_name.startAnimation(anim_alpha);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {}
        });
        anim_alpha.setDuration(500);
        anim_alpha.setFillAfter(true);
        anim_alpha.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                app_name.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {}

            @Override
            public void onAnimationRepeat(Animation animation) {}
        });
        app_icon.startAnimation(anim_bounce);

        new Handler(Looper.getMainLooper()).postDelayed(() -> {
            Intent intent = new Intent(SplashActivity.this, MainActivity.class);
            startActivity(intent);
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            finish();
        }, SPLASH_DURATION);
    }
}