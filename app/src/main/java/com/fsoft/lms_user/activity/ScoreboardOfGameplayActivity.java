package com.fsoft.lms_user.activity;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.fsoft.lms_user.APIService;
import com.fsoft.lms_user.R;
import com.fsoft.lms_user.model.Question;
import com.fsoft.lms_user.model.Quiz;

import java.util.ArrayList;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ScoreboardOfGameplayActivity extends AppCompatActivity {
    private ImageView btnQuitGame;
    private TextView tvCurrentScore;
    private Button btnNextInScoreboard;

    private String username, jwtToken;
    private ProgressDialog progressDialog;

    private String currentPosition;
    private ArrayList<Question> questionsList;
    private String currentScore;

    //--------------------------------------------------------------------------------
    HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY);

    OkHttpClient okHttpClient = new OkHttpClient.Builder()
            .addInterceptor(loggingInterceptor)
            .build();

    Retrofit retrofit = new Retrofit.Builder()
            .baseUrl("https://backend-lms.herokuapp.com/")
            .addConverterFactory(GsonConverterFactory.create())
            .client(okHttpClient)
            .build();

    APIService apiService = retrofit.create(APIService.class);

    //--------------------------------------------------------------------------------
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scoreboard_of_gameplay);

        //Get data from previous activity
        Intent intentGetData = getIntent();
        questionsList = (ArrayList<Question>) intentGetData.getSerializableExtra("questionsList");
        System.out.println(questionsList.toString());
        currentPosition = intentGetData.getStringExtra("currentPosition");
        currentScore = intentGetData.getStringExtra("currentScore");

        //Initialize all widgets ids
        btnQuitGame = findViewById(R.id.btnQuitGame);
        tvCurrentScore = findViewById(R.id.tvCurrentScore);
        btnNextInScoreboard = findViewById(R.id.btnNextInScoreboard);

        //Set up score
        tvCurrentScore.setText(currentScore);

        //Initialize loading dialog
        progressDialog = new ProgressDialog(this);

        //Set behaviour for button quit game
        btnQuitGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                quitGame();
            }
        });

        //Set behaviour for button next
        btnNextInScoreboard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = Integer.parseInt(currentPosition);

                if (position == questionsList.size()) {
                    Toast.makeText(ScoreboardOfGameplayActivity.this, "You have completed all the questions", Toast.LENGTH_LONG).show();
                } else {

                    String nextQuestionType = questionsList.get(position).getType();
                    if (!nextQuestionType.equals("multiple-choice")){
                        Toast.makeText(ScoreboardOfGameplayActivity.this, "The next question type is currently not supported, please come back another time", Toast.LENGTH_LONG).show();
                    } else {
                        Intent intent = new Intent(ScoreboardOfGameplayActivity.this, TestGamePlayActivity.class);
                        intent.putExtra("questionsList", questionsList);
                        intent.putExtra("currentScore", currentScore);

                        String strPosition = Integer.toString(position);

                        intent.putExtra("currentPosition", strPosition);

                        startActivity(intent);
                    }
                }
            }
        });

        //Get JWT token to access APIs
        getJWTToken();
    }

    //--------------------------------------------------------------------------------
    private void getJWTToken() {
        SharedPreferences loginPref = getSharedPreferences("Login", MODE_PRIVATE);
        username = loginPref.getString("loggedInUser", null);
        jwtToken = loginPref.getString("accessToken", null);

    }

    private void showProgressDialog(String text) {
        progressDialog.setTitle(text);
        progressDialog.show();
    }

    //--------------------------------------------------------------------------------
    private void quitGame() {
        new AlertDialog.Builder(ScoreboardOfGameplayActivity.this)
                .setIcon(android.R.drawable.ic_delete)
                .setTitle("Quit game")
                .setMessage("If you quit now, your score will not be saved. Quit anyway?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(ScoreboardOfGameplayActivity.this, MainActivity.class);
                        startActivity(intent);
                        finish();
                    }
                })
                .setNegativeButton("No", null)
                .show();
    }
}