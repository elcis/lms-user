package com.fsoft.lms_user.activity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.fsoft.lms_user.abtract.TextChangedListener;
import com.fsoft.lms_user.callback.MainCallbacks;
import com.fsoft.lms_user.component.CuteSnackbar;
import com.fsoft.lms_user.fragment.OpenEndedFragment;
import com.fsoft.lms_user.R;
import com.fsoft.lms_user.fragment.CheckboxFragment;
import com.fsoft.lms_user.fragment.FillBlankFragment;
import com.fsoft.lms_user.fragment.MultipleChoiceFragment;
import com.fsoft.lms_user.fragment.TrueFalseFragment;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

import static com.fsoft.lms_user.activity.CreateQuizActivity.PICK_IMAGE_REQUEST;
import static com.fsoft.lms_user.activity.MainActivity.QUESTION_URL;
import static com.fsoft.lms_user.activity.MainActivity.accessToken;
import static com.fsoft.lms_user.activity.MainActivity.loggedInUser;

@SuppressWarnings("FieldCanBeLocal")
public class AddQuestionActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener, MainCallbacks {
    // GUI elements
    private LinearLayout btnAddImage;
    private NestedScrollView scrollView;
    private TextInputLayout textInputQuestion, textInputExplanation;
    private TextInputEditText editTextQuestion, editTextExplanation;
    private Spinner spinnerQuestionType, spinner_timer;
    private ImageButton btnBackFromActivityAddQuestion, btnAddQuestion;
    private ImageView imageView;
    private Button action_category;

    // Fragment
    private FrameLayout frameLayout;
    private MultipleChoiceFragment multiChoiceFrag;
    private CheckboxFragment checkboxFrag;
    private FillBlankFragment fillBlankFrag;
    private OpenEndedFragment openEndedFrag;
    private TrueFalseFragment trueFalseFrag;

    // Data
    private final String[] questionTypes = {
            "Multiple Choice", "Checkbox",
            "Fill-in-the-Blank", "Open-ended",
            "True/False"
    };
    private static final String[] timerValues = {
            "5 Seconds", "10 Seconds", "20 Seconds",
            "30 Seconds", "60 Seconds", "90 Seconds"
    };

    // API
    // Note: Type
    private RequestQueue requestQueue;
    private final String[] types = {"multiple-choice", "checkbox", "fill-in-the-blank", "open-ended", "true-false"},
            times = {"5", "10", "20", "30", "60", "90"};
    HashMap<String, String> question;
    private Uri imageUri;
    private StorageReference storageReference;

    // Debugging
    private final String TAG = "ADD_QUESTION";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_question);

        // Set up toolbar and buttons
        btnBackFromActivityAddQuestion = findViewById(R.id.btnBackFromActivityAddQuestion);
        btnBackFromActivityAddQuestion.setOnClickListener(this::onClick);
        btnAddQuestion = findViewById(R.id.btnAddQuestion);
        btnAddQuestion.setOnClickListener(this::onClick);
        action_category = findViewById(R.id.action_category);
        action_category.setOnClickListener(this::onClick);

        // Set up image upload
        btnAddImage = findViewById(R.id.btnAddImage);
        btnAddImage.setOnClickListener(this::onClick);
        imageView = findViewById(R.id.imageView);

        // Set up question type spinner
        spinnerQuestionType = findViewById(R.id.spinnerQuestionType);
        ArrayAdapter<String> typeAdapter =
                new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, questionTypes);
        spinnerQuestionType.setAdapter(typeAdapter);
        spinnerQuestionType.setOnItemSelectedListener(this);

        // Set up timer spinner
        spinner_timer = findViewById(R.id.spinner_timer);
        ArrayAdapter<String> timerAdapter =
                new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, timerValues);
        spinner_timer.setAdapter(timerAdapter);
        spinner_timer.setOnItemSelectedListener(this);
        spinner_timer.setSelection(3);

        // Set up inputs
        textInputQuestion = findViewById(R.id.textInputQuestion);
        editTextQuestion = findViewById(R.id.editTextQuestion);
        editTextQuestion.setOnFocusChangeListener((v, focused)
                -> onFocusChange(textInputQuestion, focused, R.string.hint_question));
        editTextQuestion.addTextChangedListener(
                new TextChangedListener<TextInputEditText>(editTextQuestion) {
                    @Override
                    public void onTextChanged(TextInputEditText target, Editable s) {
                        AddQuestionActivity.this.onTextChanged(target, s);
                    }
                });
        textInputExplanation = findViewById(R.id.textInputExplanation);
        editTextExplanation = findViewById(R.id.editTextExplanation);
        editTextExplanation.addTextChangedListener(
                new TextChangedListener<TextInputEditText>(editTextExplanation) {
            @Override
            public void onTextChanged(TextInputEditText target, Editable s) {
                AddQuestionActivity.this.onTextChanged(target, s);
            }
        });

        // Set up answer fragment
        scrollView = findViewById(R.id.scrollView);
        frameLayout = findViewById(R.id.frameLayout);
        multiChoiceFrag = MultipleChoiceFragment.newInstance();
        checkboxFrag = CheckboxFragment.newInstance();
        fillBlankFrag = FillBlankFragment.newInstance();
        openEndedFrag = OpenEndedFragment.newInstance();
        trueFalseFrag = TrueFalseFragment.newInstance();
        switchFragment(multiChoiceFrag);

        // Set up question data
        question = new HashMap<String, String>() {
            {
                put("username", loggedInUser);
                put("category", "none");
                put("type", types[0]);
                put("time", times[3]);
                put("image", "NONE");
                put("question", "");
                put("answer", "[\"\",\"\",\"\",\"\"]");
                put("correct_answer", "[\"0\"]");
                put("description", "");
            }
        };

        // Set up API
        requestQueue = Volley.newRequestQueue(this);
    }

    @SuppressLint("NonConstantResourceId")
    private void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnBackFromActivityAddQuestion:
                onBackPressed();
                break;
            case R.id.btnAddQuestion:
                addQuestion();
                break;
            case R.id.action_category:
                // TODO: Set category
                break;
            case R.id.btnAddImage:
                selectThenUploadImage();
                break;
            default:
                Log.w(TAG, "onClick unknown button id '" + view.getId() + "'");
                break;
        }
    }

    private boolean onFocusChange(TextInputLayout inputLayout, boolean focused, @StringRes int stringId) {
        if (!focused) {
            String str = Objects.requireNonNull(inputLayout.getEditText()).getText().toString();
            if (str.isEmpty()) {
                inputLayout.setHint(stringId);
                inputLayout.setBackground(
                        AppCompatResources.getDrawable(
                                AddQuestionActivity.this,
                                R.drawable.rounded_input_negative
                        )
                );
                return false;
            } else {
                inputLayout.setHint("");
                inputLayout.setBackground(
                        AppCompatResources.getDrawable(
                                AddQuestionActivity.this,
                                R.drawable.rounded_input
                        )
                );
            }
        }
        return true;
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        if (adapterView.getId() == R.id.spinnerQuestionType) {
            switchFragment(i);
            question.put("type", types[i]);
        } else if (adapterView.getId() == R.id.spinner_timer) {
            question.put("time", times[i]);
        }
    }

    private void onTextChanged(TextInputEditText target, Editable s) {
        if (target.getId() == R.id.editTextQuestion) {
            question.put("question", s.toString());
        } else if (target.getId() == R.id.editTextExplanation) {
            question.put("description", s.toString());
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {}

    private void addQuestion() {
        // Check if some fields are empty
        if (!onFocusChange(textInputQuestion, false, R.string.hint_question) ||
            Objects.requireNonNull(question.get("answer")).contains("\"\"")) {
            Bundle bundle = new Bundle();
            bundle.putString("action", "checkAnswers");
            switch (spinnerQuestionType.getSelectedItemPosition()) {
                case 0:
                    multiChoiceFrag.mainToFrag(bundle);
                    break;
                case 1:
                    checkboxFrag.mainToFrag(bundle);
                    break;
                case 2:
                    fillBlankFrag.mainToFrag(bundle);
                    break;
                case 3:
                    openEndedFrag.mainToFrag(bundle);
                    break;
                case 4:
                    trueFalseFrag.mainToFrag(bundle);
                    break;
            }
            return;
        }

        // Set up Snackbar
        CuteSnackbar snackbar = new CuteSnackbar(this, scrollView);
        CuteSnackbar snackbarLoading = new CuteSnackbar(this, scrollView, Snackbar.LENGTH_INDEFINITE);
        snackbarLoading.setContent(R.drawable.loading, R.string.info_adding_question);

        // Hide keyboard
        // https://stackoverflow.com/a/17789187
        InputMethodManager imm = (InputMethodManager) this.getSystemService(AppCompatActivity.INPUT_METHOD_SERVICE);
        View v = this.getCurrentFocus();
        if (v == null) {
            v = new View(this);
        }
        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

        // Set result for CreateQuizActivity
        JSONObject jsonObject = new JSONObject(question);
        try {
            jsonObject.put("answer",
                    new JSONArray(Objects.requireNonNull(question.get("answer")).replace("\\\"","\"")));
            jsonObject.put("correct_answer",
                    new JSONArray(Objects.requireNonNull(question.get("correct_answer")).replace("\\\"", "\"")));
        } catch (JSONException ignored) {}
        Log.i(TAG, "addQuestion generated json: " + jsonObject);

        // Send POST request
        JsonObjectRequest stringRequest = new JsonObjectRequest(
                Request.Method.POST,
                QUESTION_URL,
                jsonObject,
                response -> {
                    try {
                        if (response.has("message")) {
                            throw new Throwable(response.getString("message"));
                        }
                        Log.i(TAG, "addQuestion received response: " + response);
                        snackbarLoading.dismiss();
                        snackbar.setContent(R.drawable.checkmark, R.string.info_addQuestion_success)
                                .show();

                        // Return data and end activity
                        Bundle bundle = new Bundle();
                        bundle.putString("id", response.getString("_id"));
                        bundle.putString("type", question.get("type"));
                        bundle.putString("image", question.get("image"));
                        bundle.putString("question", question.get("question"));
                        Intent intent = new Intent();
                        intent.putExtras(bundle);
                        setResult(RESULT_OK, intent);
                        finish();
                    } catch (Throwable e) {
                        Log.e(TAG, "addQuestion met with an error: " + e.getMessage());
                        snackbar.setContent(R.drawable.exclamation, R.string.info_addQuestion_failed)
                                .setAction(R.string.action_retry, view -> addQuestion())
                                .show();
                    }
                }, error -> {
                    snackbarLoading.dismiss();
                    snackbar.setContent(R.drawable.exclamation, R.string.info_addQuestion_failed)
                            .setAction(R.string.action_retry, view -> addQuestion())
                            .show();
                }) {

                @Override
                public Map<String, String> getHeaders() {
                    return new HashMap<String, String>() {
                        {
                            put("access-token", accessToken);
                        }
                    };
                }
        };
        stringRequest.setTag(TAG);
        requestQueue.add(stringRequest);
        snackbarLoading.show();
    }

    private void selectThenUploadImage() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, PICK_IMAGE_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && data != null && data.getData() != null) {
            imageUri = data.getData();
            btnAddImage.setPadding(0, 0, 0, 0);
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            imageView.setImageURI(imageUri);

            uploadImage();
        }
    }

    private void uploadImage() {
        CuteSnackbar snackbar = new CuteSnackbar(this, scrollView),
                snackbarLoading = new CuteSnackbar(this, scrollView, Snackbar.LENGTH_INDEFINITE);
        snackbarLoading.setContent(R.drawable.loading, R.string.info_uploading)
                .show();

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss", Locale.CHINA);
        Date now = new Date();
        String fileName = loggedInUser + "_" + simpleDateFormat.format(now);

        // Upload selected cover image to firebase storage
        storageReference = FirebaseStorage.getInstance().getReference("images/" + fileName);
        storageReference.putFile(imageUri)
                .addOnSuccessListener(taskSnapshot -> {
                    snackbarLoading.dismiss();
                    snackbar.setContent(R.drawable.checkmark, R.string.info_upload_success).show();

                    // Get image url after upload image to firebase storage
                    storageReference.getDownloadUrl().addOnSuccessListener(uri ->
                            question.put("image", uri.toString())).addOnFailureListener(e ->
                            snackbar.setContent(R.drawable.checkmark, R.string.info_upload_fail_url).show());
                })
                .addOnFailureListener(e -> {
                    snackbarLoading.dismiss();
                    snackbar.setContent(R.drawable.checkmark, R.string.info_upload_fail).show();
                });
    }

    private void switchFragment(Fragment fragment) {
        if (fragment != null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.frameLayout, fragment)
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                    .commit();
        }
    }

    private void switchFragment(int id) {
        switch (id) {
            case 0:
                switchFragment(multiChoiceFrag);
                break;
            case 1:
                switchFragment(checkboxFrag);
                break;
            case 2:
                switchFragment(fillBlankFrag);
                break;
            case 3:
                switchFragment(openEndedFrag);
                break;
            case 4:
                switchFragment(trueFalseFrag);
                break;
            default:
                Log.e(TAG, "onItemSelected encountered an error: Unknown option number " + id);
                break;
        }
    }

    @Override
    public void fragToMain(String caller, Bundle bundle) {
        if (Arrays.asList(types).contains(caller)) {
            if (bundle.containsKey("key") && bundle.containsKey("val")) {
                question.put(bundle.getString("key"), bundle.getString("val"));
            } else {
                Log.e(TAG, "fragToMain received invalid bundle: " + bundle);
            }
        } else {
            Log.e(TAG, "Calling fragToMain from unknown caller: " + caller);
        }
    }
}