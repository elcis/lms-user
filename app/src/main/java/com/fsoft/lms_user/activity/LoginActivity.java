package com.fsoft.lms_user.activity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.util.TypedValue;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.annotation.StringRes;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.appcompat.widget.Toolbar;
import androidx.core.widget.TextViewCompat;

import com.android.volley.Request;
import com.android.volley.RequestQueue;

import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.fsoft.lms_user.R;
import com.fsoft.lms_user.callback.MainCallbacks;
import com.fsoft.lms_user.component.CuteSnackbar;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;

import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

import static com.fsoft.lms_user.activity.MainActivity.LOGIN_URL;
import static com.fsoft.lms_user.activity.MainActivity.SIGNUP_URL;
import static com.fsoft.lms_user.activity.MainActivity.loginPref;

@SuppressWarnings("FieldCanBeLocal")
public class LoginActivity extends AppCompatActivity implements MainCallbacks {

    // GUI Elements
    private Toolbar toolbar;
    private ScrollView scrollView;
    private RelativeLayout loginLayout, signupLayout, signupProfileLayout;
    private LinearLayout buttonLayout, loginWithLayout;
    private TextView textBanner, info_or, info_user_description;
    private TextInputLayout textInputUsername, textInputPassword, textInputConfirm,
            textInputName, textInputEmail, textInputDob;
    private TextInputEditText editTextUsername, editTextPassword, editTextConfirm,
            editTextName, editTextEmail, editTextDob;
    private Button action_login, action_signup,
            action_with_google, action_with_facebook,
            action_signup_single, action_signup_finish,
            action_lecturer, action_student;

    // Activities
    private ActivitySwitcher activitySwitcher;
    private String main_frag;

    // APIs
    private RequestQueue requestQueue;
    private FirebaseAuth firebaseAuth;
    private CallbackManager callbackManager;

    // Sign up data
    private boolean role = false;
    private Date dob;

    // Debug
    private final String TAG = "LoginActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        // Set up ActivitySwitcher
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        activitySwitcher = new ActivitySwitcher(this, "login");
        main_frag = bundle.getString("caller");
        activitySwitcher.setResult(main_frag);

        // Set up toolbar
        toolbar = findViewById(R.id.toolbar);
        toolbar.setNavigationOnClickListener(view -> onBackPressed());

        // Set up form
        scrollView = findViewById(R.id.scrollView);
        // Layouts
        loginLayout = findViewById(R.id.loginLayout);
        signupLayout = findViewById(R.id.signupLayout);
        signupProfileLayout = findViewById(R.id.signupProfileLayout);
        buttonLayout = findViewById(R.id.buttonLayout);
        loginWithLayout = findViewById(R.id.loginWithLayout);
        // Login text fields
        textBanner = findViewById(R.id.textBanner);
        info_or = findViewById(R.id.info_or);
        textInputUsername = findViewById(R.id.textInputUsername);
        editTextUsername = findViewById(R.id.editTextUsername);
        editTextUsername.setOnFocusChangeListener((view, focused)
                -> onFocusChange(textInputUsername, focused, R.string.hint_username));
        textInputPassword = findViewById(R.id.textInputPassword);
        editTextPassword = findViewById(R.id.editTextPassword);
        editTextPassword.setOnFocusChangeListener((view, focused)
                -> onFocusChange(textInputPassword, focused, R.string.hint_password));
        // Signup text fields
        info_user_description = findViewById(R.id.info_user_description);
        textInputConfirm = findViewById(R.id.textInputConfirm);
        editTextConfirm = findViewById(R.id.editTextConfirm);
        editTextConfirm.setOnFocusChangeListener((view, focused)
                -> onFocusChange(textInputConfirm, focused, R.string.hint_confirm));
        textInputName = findViewById(R.id.textInputName);
        editTextName = findViewById(R.id.editTextName);
        editTextName.setOnFocusChangeListener((view, focused)
                -> onFocusChange(textInputName, focused, R.string.hint_name));
        textInputEmail = findViewById(R.id.textInputEmail);
        editTextEmail = findViewById(R.id.editTextEmail);
        editTextEmail.setOnFocusChangeListener((view, focused)
                -> onFocusChange(textInputEmail, focused, R.string.hint_email));
        textInputDob = findViewById(R.id.textInputDob);
        editTextDob = findViewById(R.id.editTextDob);
        editTextDob.setOnFocusChangeListener((view, focused)
                -> onFocusChange(textInputDob, focused, R.string.hint_dob));
        editTextDob.setOnClickListener(this::onClick);
        // Login layout buttons
        action_login = findViewById(R.id.action_login);
        action_login.setOnClickListener(this::onClick);
        action_signup = findViewById(R.id.action_signup);
        action_signup.setOnClickListener(this::onClick);
        action_with_google = findViewById(R.id.action_with_google);
        action_with_google.setOnClickListener(this::onClick);
        action_with_facebook = findViewById(R.id.action_with_facebook);
        action_with_facebook.setOnClickListener(this::onClick);
        // Signup buttons
        action_signup_single = findViewById(R.id.action_signup_single);
        action_signup_single.setOnClickListener(this::onClick);
        action_signup_finish = findViewById(R.id.action_signup_finish);
        action_signup_finish.setOnClickListener(this::onClick);
        action_lecturer = findViewById(R.id.action_lecturer);
        action_lecturer.setOnClickListener(this::onClick);
        action_student = findViewById(R.id.action_student);
        action_student.setOnClickListener(this::onClick);

        // Set up API
        requestQueue = Volley.newRequestQueue(this);
        firebaseAuth = FirebaseAuth.getInstance();
        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Bundle bundle = new Bundle();
                loginFacebook(bundle);
            }

            @Override
            public void onCancel() {
                Bundle bundle = new Bundle();
                bundle.putString("err", "User canceled Facebook login prompt.");
                loginFacebook(bundle);
            }

            @Override
            public void onError(FacebookException error) {
                Bundle bundle = new Bundle();
                bundle.putString("err", error.getMessage());
                loginFacebook(bundle);
            }
        });
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (requestQueue != null) {
            requestQueue.cancelAll(TAG);
        }
    }

    @SuppressLint("NonConstantResourceId")
    private void onClick(View view) {
        switch (view.getId()) {
            case R.id.action_login:
                login();
                break;
            case R.id.action_signup:
                switchLayout("signup");
                break;
            case R.id.action_with_google:
                loginGoogle();
                break;
            case R.id.action_with_facebook:
                loginFacebook();
                break;

            case R.id.action_signup_single:
                signup();
                break;
            case R.id.editTextDob:
                // https://stackoverflow.com/a/14933515
                final Calendar calendar = Calendar.getInstance();
                new DatePickerDialog(
                        this,
                        (datePicker, y, m, d) -> {
                            calendar.set(Calendar.YEAR, y);
                            calendar.set(Calendar.MONTH, m);
                            calendar.set(Calendar.DAY_OF_MONTH, d);
                            SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy", Locale.ROOT);
                            editTextDob.setText(format.format(calendar.getTime()));
                            dob = calendar.getTime();
                        },
                        calendar.get(Calendar.YEAR),
                        calendar.get(Calendar.MONTH),
                        calendar.get(Calendar.DAY_OF_MONTH)).show();
                break;
            case R.id.action_lecturer:
                role = true;
                info_user_description.setText(R.string.info_user_description_lecturer);
                break;
            case R.id.action_student:
                role = false;
                info_user_description.setText(R.string.info_user_description_student);
                break;
            case R.id.action_signup_finish:
                signupProfile();
                break;
            default:
                Log.w(TAG, "onClick unknown button '" + ((Button) view).getText() + "'");
                break;
        }
    }

    private boolean onFocusChange(TextInputLayout inputLayout, boolean focused, @StringRes int stringId) {
        if (!focused) {
            String str = Objects.requireNonNull(inputLayout.getEditText()).getText().toString();
            if (str.isEmpty()) {
                inputLayout.setHint(stringId);
                inputLayout.setBackground(
                        AppCompatResources.getDrawable(
                                LoginActivity.this,
                                R.drawable.rounded_input_negative
                        )
                );
                return false;
            } else {
                inputLayout.setHint("");
                inputLayout.setBackground(
                        AppCompatResources.getDrawable(
                                LoginActivity.this,
                                R.drawable.rounded_input
                        )
                );
            }
        }
        return true;
    }

    private void login() {
        // https://developer.android.com/training/volley
        String username = Objects.requireNonNull(editTextUsername.getText()).toString(),
                password = Objects.requireNonNull(editTextPassword.getText()).toString();

        // Check if some fields are empty
        if (!onFocusChange(textInputUsername, false, R.string.hint_username) |
                !onFocusChange(textInputPassword, false, R.string.hint_password)) {
            return;
        }

        // Set up Snackbar
        CuteSnackbar snackbar = new CuteSnackbar(this, scrollView);
        CuteSnackbar snackbarLoading = new CuteSnackbar(this, scrollView, Snackbar.LENGTH_INDEFINITE);
        snackbarLoading.setContent(R.drawable.loading, R.string.info_logging);

        // Hide keyboard
        // https://stackoverflow.com/a/17789187
        InputMethodManager imm = (InputMethodManager) this.getSystemService(AppCompatActivity.INPUT_METHOD_SERVICE);
        View v = this.getCurrentFocus();
        if (v == null) {
            v = new View(this);
        }
        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

        // Check if password is too short
        if (password.length() < 6) {
            textInputPassword.setHint(R.string.hint_password);
            textInputPassword.setBackground(
                    AppCompatResources.getDrawable(
                            LoginActivity.this,
                            R.drawable.rounded_input_negative
                    )
            );
            snackbar.setContent(R.drawable.exclamation, R.string.info_login_short_password);
            snackbar.show();
            return;
        } else {
            textInputPassword.setHint(R.string.hint_password);
            textInputPassword.setBackground(
                    AppCompatResources.getDrawable(
                            LoginActivity.this,
                            R.drawable.rounded_input
                    )
            );
        }

        // Send POST request
        StringRequest jsonRequest = new StringRequest(
                Request.Method.POST,
                LOGIN_URL,
                response -> {
                    Log.i(TAG, "Login received response: " + response);
                    snackbarLoading.dismiss();
                    snackbar.setContent(R.drawable.checkmark, R.string.info_login_success)
                            .show();

                    // Return to MainActivity
                    loginPref.edit().putString("loggedInUser", username).apply();
                    loginPref.edit().putString("accessToken", response).apply();
                    activitySwitcher.backToMain(main_frag);
                }, error -> {
            int strRes;
            try {
                // Get status code and body
                String body = "null",
                        statusCode = String.valueOf(error.networkResponse.statusCode);
                if (error.networkResponse.data != null) {
                    try {
                        body = new String(error.networkResponse.data, StandardCharsets.UTF_8);
                    } catch (Exception ignored) {
                    }
                }
                Log.e(TAG, "Login response produced error: " + statusCode + " - " + body);

                // Check error response
                if (body.equals(getString(R.string.api_res_no_user))) {
                    strRes = R.string.info_login_no_user;
                } else if (body.equals(getString(R.string.api_res_wrong_password))) {
                    strRes = R.string.info_login_wrong_password;
                } else {
                    strRes = R.string.info_login_failed;
                }
            } catch (Exception e) {
                strRes = R.string.info_login_failed;
            }

            snackbarLoading.dismiss();
            snackbar.setContent(R.drawable.exclamation, strRes);
            if (strRes == R.string.info_login_failed) {
                snackbar.setAction(R.string.action_retry, view -> login());
            }
            snackbar.show();
        }) {

            @Override
            protected Map<String, String> getParams() {
                return new HashMap<String, String>() {
                    {
                        put("username", username);
                        put("password", password);
                    }
                };
            }

            @Override
            public Map<String, String> getHeaders() {
                return new HashMap<String, String>() {
                    {
                        put("Content-Type", "application/x-www-form-urlencoded");
                    }
                };
            }
        };
        jsonRequest.setTag(TAG);
        requestQueue.add(jsonRequest);
        snackbarLoading.show();
    }

    private void signup() {
        String username = Objects.requireNonNull(editTextUsername.getText()).toString(),
                password = Objects.requireNonNull(editTextPassword.getText()).toString(),
                confirm = Objects.requireNonNull(editTextConfirm.getText()).toString();

        // Check if some fields are empty
        if (!onFocusChange(textInputUsername, false, R.string.hint_username) |
                !onFocusChange(textInputPassword, false, R.string.hint_password) |
                !onFocusChange(textInputConfirm, false, R.string.hint_confirm)) {
            return;
        }

        // Check if both password fields are the same
        if (!password.equals(confirm)) {
            textInputPassword.setBackground(
                    AppCompatResources.getDrawable(
                            LoginActivity.this,
                            R.drawable.rounded_input_negative
                    )
            );
            textInputConfirm.setBackground(
                    AppCompatResources.getDrawable(
                            LoginActivity.this,
                            R.drawable.rounded_input_negative
                    )
            );
            return;
        } else {
            textInputPassword.setBackground(
                    AppCompatResources.getDrawable(
                            LoginActivity.this,
                            R.drawable.rounded_input
                    )
            );
            textInputConfirm.setBackground(
                    AppCompatResources.getDrawable(
                            LoginActivity.this,
                            R.drawable.rounded_input
                    )
            );
        }

        // Set up Snackbar
        CuteSnackbar snackbar = new CuteSnackbar(this, scrollView);
        CuteSnackbar snackbarLoading = new CuteSnackbar(this, scrollView, Snackbar.LENGTH_INDEFINITE);
        snackbarLoading.setContent(R.drawable.loading, R.string.info_signing);

        // Hide keyboard
        InputMethodManager imm = (InputMethodManager) this.getSystemService(AppCompatActivity.INPUT_METHOD_SERVICE);
        View v = this.getCurrentFocus();
        if (v == null) {
            v = new View(this);
        }
        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

        // Check if password is too short
        if (password.length() < 6) {
            textInputPassword.setHint(R.string.hint_password);
            textInputPassword.setBackground(
                    AppCompatResources.getDrawable(
                            LoginActivity.this,
                            R.drawable.rounded_input_negative
                    )
            );
            snackbar.setContent(R.drawable.exclamation, R.string.info_login_short_password)
                    .show();
            return;
        } else {
            textInputPassword.setHint(R.string.hint_password);
            textInputPassword.setBackground(
                    AppCompatResources.getDrawable(
                            LoginActivity.this,
                            R.drawable.rounded_input
                    )
            );
        }

        // Check if username already exist
        StringRequest jsonRequest = new StringRequest(
                Request.Method.POST,
                LOGIN_URL,
                response -> {
                    Log.i(TAG, "Login (user-check) received response: " + response);
                    snackbarLoading.dismiss();
                    snackbar.setContent(R.drawable.exclamation, R.string.info_signup_exist_user)
                            .show();
                }, error -> {
            try {
                // Get status code and body
                String body = "null",
                        statusCode = String.valueOf(error.networkResponse.statusCode);
                if (error.networkResponse.data != null) {
                    try {
                        body = new String(error.networkResponse.data, StandardCharsets.UTF_8);
                    } catch (Exception ignored) {
                    }
                }
                Log.e(TAG, "Login (user-check) response produced error: " + statusCode + " - " + body);

                // Check error response
                if (body.equals(getString(R.string.api_res_no_user))) {
                    // Go to SignUpProfile
                    snackbarLoading.dismiss();
                    switchLayout("profile");
                    return;
                } else if (body.equals(getString(R.string.api_res_wrong_password))) {
                    snackbarLoading.dismiss();
                    snackbar.setContent(R.drawable.exclamation, R.string.info_signup_exist_user)
                            .show();
                    return;
                }
            } catch (Exception ignored) {}

            snackbarLoading.dismiss();
            snackbar.setContent(R.drawable.exclamation, R.string.info_signup_failed)
                    .setAction(R.string.action_retry, view -> signup())
                    .show();
        }) {

            @Override
            protected Map<String, String> getParams() {
                return new HashMap<String, String>() {
                    {
                        put("username", username);
                        put("password", password);
                    }
                };
            }

            @Override
            public Map<String, String> getHeaders() {
                return new HashMap<String, String>() {
                    {
                        put("Content-Type", "application/x-www-form-urlencoded");
                    }
                };
            }
        };

        jsonRequest.setTag(TAG);
        requestQueue.add(jsonRequest);
        snackbarLoading.show();
    }

    private void signupProfile() {
        String username = Objects.requireNonNull(editTextUsername.getText()).toString(),
                password = Objects.requireNonNull(editTextPassword.getText()).toString(),
                name = Objects.requireNonNull(editTextName.getText()).toString(),
                email = Objects.requireNonNull(editTextEmail.getText()).toString(),
                dobString = Objects.requireNonNull(editTextDob.getText()).toString();

        // Check if some fields are empty
        if (!onFocusChange(textInputName, false, R.string.hint_name) |
                !onFocusChange(textInputEmail, false, R.string.hint_email) |
                !onFocusChange(textInputDob, false, R.string.hint_dob)) {
            return;
        }

        // Set up Snackbar
        CuteSnackbar snackbar = new CuteSnackbar(this, scrollView);
        CuteSnackbar snackbarLoading = new CuteSnackbar(this, scrollView, Snackbar.LENGTH_INDEFINITE);
        snackbarLoading.setContent(R.drawable.loading, R.string.info_signup_checking);

        // Check if email is valid
        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            textInputEmail.setBackground(
                    AppCompatResources.getDrawable(
                            LoginActivity.this,
                            R.drawable.rounded_input_negative
                    )
            );
            return;
        } else {
            textInputEmail.setBackground(
                    AppCompatResources.getDrawable(
                            LoginActivity.this,
                            R.drawable.rounded_input
                    )
            );
        }

        // Check if dob is valid (before today)
        Date today = Date.from(Instant.now());
        if (dob.after(today)) {
            textInputDob.setBackground(
                    AppCompatResources.getDrawable(
                            LoginActivity.this,
                            R.drawable.rounded_input_negative
                    )
            );
            return;
        } else {
            textInputDob.setBackground(
                    AppCompatResources.getDrawable(
                            LoginActivity.this,
                            R.drawable.rounded_input
                    )
            );
        }

        // Hide keyboard
        // https://stackoverflow.com/a/17789187
        InputMethodManager imm = (InputMethodManager) this.getSystemService(AppCompatActivity.INPUT_METHOD_SERVICE);
        View v = this.getCurrentFocus();
        if (v == null) {
            v = new View(this);
        }
        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

        // Send POST request
        StringRequest jsonRequest = new StringRequest(
                Request.Method.POST,
                SIGNUP_URL,
                response -> {
                    try {
                        try {
                            JSONObject obj = new JSONObject(response);
                            if (obj.has("message")) {
                                throw new Throwable(obj.getString("message"));
                            }
                        } catch (JSONException ignored) {
                        } catch (Throwable e) {
                            throw new Throwable(e.getMessage());
                        }
                        Log.i(TAG, "Sign up received response: " + response);
                        snackbarLoading.dismiss();
                        snackbar.setContent(R.drawable.checkmark, R.string.info_signup_success);
                        snackbar.show();

                        // Perform Login
                        editTextUsername.setText(username);
                        editTextPassword.setText(password);
                        login();
                    } catch (Throwable e) {
                        Log.e(TAG, "Profile met with an error: " + e.getMessage());
                        snackbar.setContent(R.drawable.exclamation, R.string.info_signup_failed)
                                .show();
                    }
                }, error -> {
            int strRes;
            try {
                // Get status code and body
                String body = "null",
                        statusCode = String.valueOf(error.networkResponse.statusCode);
                if (error.networkResponse.data != null) {
                    try {
                        body = new String(error.networkResponse.data, StandardCharsets.UTF_8);
                    } catch (Exception ignored) {
                    }
                }
                Log.e(TAG, "Sign up response produced error: " + statusCode + " - " + body);

                // Check error response
                if (body.equals(getString(R.string.api_res_exist_user))) {
                    strRes = R.string.info_signup_exist_user;
                } else if (body.equals(getString(R.string.api_res_exist_email))) {
                    strRes = R.string.info_signup_exist_email;
                } else {
                    strRes = R.string.info_signup_failed;
                }
            } catch (Exception e) {
                strRes = R.string.info_signup_failed;
            }

            snackbarLoading.dismiss();
            snackbar.setContent(R.drawable.exclamation, strRes);
            if (strRes == R.string.info_signup_failed) {
                snackbar.setAction(R.string.action_retry, view -> signup());
            }
            snackbar.show();
        }) {

            @Override
            protected Map<String, String> getParams() {
                return new HashMap<String, String>() {
                    {
                        put("username", username);
                        put("password", password);
                        put("name", name);
                        put("email", email);
                        put("type", role ? "true" : "false");
                        put("birthyear", dobString);
                    }
                };
            }

            @Override
            public Map<String, String> getHeaders() {
                return new HashMap<String, String>() {
                    {
                        put("Content-Type", "application/x-www-form-urlencoded");
                    }
                };
            }
        };

        jsonRequest.setTag(TAG);
        requestQueue.add(jsonRequest);
        snackbarLoading.show();
    }

    // Perform login or signup depending on user existing or not
    private void loginOrSignup(String username, String password) {
        // Set up Snackbar
        CuteSnackbar snackbar = new CuteSnackbar(this, scrollView);
        CuteSnackbar snackbarLoading = new CuteSnackbar(this, scrollView, Snackbar.LENGTH_INDEFINITE);
        snackbarLoading.setContent(R.drawable.loading, R.string.info_logging);

        // Send POST request
        StringRequest jsonRequest = new StringRequest(
                Request.Method.POST,
                LOGIN_URL,
                response -> {
                    Log.i(TAG, "Login-or-Signup received response: " + response);
                    snackbarLoading.dismiss();

                    // Return to Main
                    loginPref.edit().putString("loggedInUser", username).apply();
                    loginPref.edit().putString("accessToken", response).apply();
                    activitySwitcher.backToMain(main_frag);
                }, error -> {
            try {
                // Get status code and body
                String body = "null",
                        statusCode = String.valueOf(error.networkResponse.statusCode);
                if (error.networkResponse.data != null) {
                    try {
                        body = new String(error.networkResponse.data, StandardCharsets.UTF_8);
                    } catch (Exception ignored) {
                    }
                }
                Log.e(TAG, "Login-or-Signup response produced error: " + statusCode + " - " + body);

                // Check error response
                if (body.equals(getString(R.string.api_res_no_user))) {
                    // Show SignUpProfile with filled in info
                    snackbarLoading.dismiss();
                    switchLayout("profile");
                    return;
                } else if (body.equals(getString(R.string.api_res_wrong_password))) {
                    snackbarLoading.dismiss();
                    snackbar.setContent(R.drawable.exclamation, R.string.info_signup_exist_user)
                            .show();
                    return;
                }
            } catch (Exception ignored) {}

            snackbarLoading.dismiss();
            snackbar.setContent(R.drawable.exclamation, R.string.info_signup_failed)
                    .setAction(R.string.action_retry, view -> signup())
                    .show();
        }) {

            @Override
            protected Map<String, String> getParams() {
                return new HashMap<String, String>() {
                    {
                        put("username", username);
                        put("password", password);
                    }
                };
            }

            @Override
            public Map<String, String> getHeaders() {
                return new HashMap<String, String>() {
                    {
                        put("Content-Type", "application/x-www-form-urlencoded");
                    }
                };
            }
        };
        jsonRequest.setTag(TAG);
        requestQueue.add(jsonRequest);
        snackbarLoading.show();
    }

    private void loginGoogle() {
        // REF: https://firebase.google.com/docs/auth/android/google-signin#java_1
        activitySwitcher.startLoginGoogle();
    }

    private void loginGoogle(Bundle bundle) {
        CuteSnackbar snackbar = new CuteSnackbar(this, scrollView);
        try {
            if (!bundle.containsKey("err")) {
                if (bundle.containsKey("idToken")) {
                    String idToken = bundle.getString("idToken");
                    AuthCredential credential = GoogleAuthProvider.getCredential(idToken, null);
                    firebaseAuth.signInWithCredential(credential)
                            .addOnCompleteListener(this, task -> {
                                try {
                                    loginFirebase(task);
                                } catch (Throwable e) {
                                    Log.e(TAG, e.getMessage());
                                    snackbar.setContent(R.drawable.exclamation, R.string.info_login_failed_google)
                                            .setAction(R.string.action_retry, view -> loginGoogle());
                                }
                            });
                } else {
                    throw new Throwable("loginGoogle results do not contain idToken");
                }
            } else {
                throw new Throwable("loginGoogle received error: " + bundle.getString("err"));
            }
        } catch (Throwable e) {
            Log.e(TAG, e.getMessage());
            snackbar.setContent(R.drawable.exclamation, R.string.info_login_failed_google)
                    .setAction(R.string.action_retry, view -> loginGoogle());
        }
        if (!snackbar.getTextView().getText().toString().equals(getString(R.string.placeholder_action))) {
            snackbar.show();
        }
    }

    private void loginFacebook() {
        // REF: https://firebase.google.com/docs/auth/android/facebook-login
        //      https://stackoverflow.com/a/39065956
        LoginManager.getInstance().logInWithReadPermissions(
                this,
                Arrays.asList("email", "public_profile")
        );
    }

    private void loginFacebook(Bundle bundle) {
        CuteSnackbar snackbar = new CuteSnackbar(this, scrollView);
        try {
            if (!bundle.containsKey("err")) {
                AccessToken accessToken = AccessToken.getCurrentAccessToken();
                AuthCredential credential = FacebookAuthProvider.getCredential(accessToken.getToken());
                firebaseAuth.signInWithCredential(credential)
                        .addOnCompleteListener(this, task -> {
                            try {
                                loginFirebase(task);
                            } catch (Throwable e) {
                                Log.e(TAG, e.getMessage());
                                snackbar.setContent(R.drawable.exclamation, R.string.info_login_failed_facebook)
                                        .setAction(R.string.action_retry, view -> loginFacebook());
                            }
                        });
            } else {
                throw new Throwable("loginFacebook received error: " + bundle.getString("err"));
            }
        } catch (Throwable e) {
            Log.e(TAG, e.getMessage());
            snackbar.setContent(R.drawable.exclamation, R.string.info_login_failed_facebook)
                    .setAction(R.string.action_retry, view -> loginFacebook());
        }
        if (!snackbar.getTextView().getText().toString().equals(getString(R.string.placeholder_action))) {
            snackbar.show();
        }
    }

    private void loginFirebase(Task<AuthResult> task) throws Throwable {
        if (task.isSuccessful()) {
            // Sign in success, update UI with the signed-in user's information
            Log.d(TAG, "signInWithCredential success");
            FirebaseUser user = firebaseAuth.getCurrentUser();
            if (user != null) {
                String username = user.getEmail(),
                        id = user.getUid(),
                        name = user.getDisplayName(),
                        email = user.getEmail();
                Log.i(TAG, String.format(
                                "Userdata: \n{\n\t" +
                                        "username: %s,\n\t" +
                                        "id: %s,\n\t" +
                                        "name: %s,\n\t" +
                                        "email: %s\n" +
                                        "}",
                                username, id, name, email
                        )
                );
                editTextUsername.setText(username);
                onFocusChange(textInputUsername,
                        false, R.string.hint_username);
                editTextPassword.setText(id);
                onFocusChange(textInputPassword,
                        false, R.string.hint_password);
                editTextConfirm.setText(id);
                onFocusChange(textInputConfirm,
                        false, R.string.hint_password);
                editTextName.setText(name);
                onFocusChange(textInputName,
                        false, R.string.hint_name);
                editTextEmail.setText(email);
                onFocusChange(textInputEmail,
                        false, R.string.hint_email);
                loginOrSignup(username, id);
            } else {
                throw new Throwable("FirebaseUser is null");
            }
        } else {
            // If sign in fails, display a message to the user.
            throw new Throwable("signInWithCredential failed" + task.getException());
        }
    }

    private void switchLayout(String layout) {
        float translation = 32f;
        long duration = 300;
        switch (layout) {
            case "login":
                break;
            case "signup":
                buttonLayout.animate().translationXBy(-translation).alpha(0).setDuration(duration)
                        .setListener(getAnimatorListener(buttonLayout, View.GONE));
                info_or.animate().translationXBy(-translation).alpha(0).setDuration(duration)
                        .setListener(getAnimatorListener(info_or, View.GONE));
                loginWithLayout.animate().translationXBy(-translation).alpha(0).setDuration(duration)
                        .setListener(getAnimatorListener(loginWithLayout, View.GONE));
                signupLayout.animate().translationXBy(translation).alpha(0).setDuration(0)
                        .setListener(new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                super.onAnimationEnd(animation);
                                signupLayout.animate().translationXBy(-translation).alpha(1).setDuration(duration)
                                        .setListener(getAnimatorListener(signupLayout, View.VISIBLE));
                            }
                        });
                break;
            case "profile":
                toolbar.setNavigationOnClickListener(null);
                toolbar.animate().alpha(0).setDuration(duration)
                        .setListener(new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                super.onAnimationEnd(animation);
                                toolbar.setNavigationIcon(R.drawable.info_profile);
                                toolbar.setTitle(R.string.info_profile_create);
                                toolbar.animate().alpha(1).setDuration(duration);
                            }
                        });
                textBanner.animate().alpha(0).setDuration(duration)
                        .setListener(new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                super.onAnimationEnd(animation);
                                textBanner.setText(R.string.info_welcome);
                                TextViewCompat.setAutoSizeTextTypeWithDefaults(textBanner, TextViewCompat.AUTO_SIZE_TEXT_TYPE_NONE);
                                textBanner.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
                                textBanner.setTypeface(Typeface.SANS_SERIF, Typeface.NORMAL);
                                textBanner.animate().alpha(1).setDuration(duration);
                            }
                        });
                loginLayout.animate().translationXBy(-translation).alpha(0).setDuration(duration)
                        .setListener(getAnimatorListener(loginLayout, View.GONE));
                signupLayout.animate().translationXBy(-translation).alpha(0).setDuration(duration)
                        .setListener(getAnimatorListener(signupLayout, View.GONE));
                signupProfileLayout.animate().translationXBy(translation).alpha(0).setDuration(0)
                        .setListener(new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                super.onAnimationEnd(animation);
                                signupProfileLayout.animate().translationXBy(-translation).alpha(1).setDuration(duration)
                                        .setListener(getAnimatorListener(signupProfileLayout, View.VISIBLE));
                            }
                        });
                break;
            default:
                Log.w(TAG, "switchLayout received unknown layout name: " + layout);
        }
    }

    private AnimatorListenerAdapter getAnimatorListener(View view, int visibility) {
        return new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                view.setVisibility(visibility);
            }
        };
    }

    @Override
    public void fragToMain(String caller, Bundle bundle) {
        if (caller.equals("google")) {
            loginGoogle(bundle);
        } else if (caller.equals("facebook")) {
            loginFacebook(bundle);
        } else {
            Log.w(TAG, "fragToMain received unknown caller: " + caller);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }
}