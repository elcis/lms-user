package com.fsoft.lms_user.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.fsoft.lms_user.APIService;
import com.fsoft.lms_user.R;
import com.fsoft.lms_user.adapter.QuestionPreviewAdapter;
import com.fsoft.lms_user.model.Question;
import com.fsoft.lms_user.model.QuestionPreview;
import com.fsoft.lms_user.model.Quiz;

import org.w3c.dom.Text;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ViewQuizDetailActivity extends AppCompatActivity {
    ProgressDialog progressDialog;

    private ImageView ivQuizImageInViewQuizDetail;
    private TextView tvNumberOfQuestionsInViewQuizDetail;
    private TextView tvQuizNameInViewQuizDetail;
    private TextView tvCategoryInViewQuizDetail;
    private TextView tvQuizAuthorInViewQuizDetail;
    private TextView tvThisQuizCurrentlyHasNoQuestion;

    private ImageView btnBackFromViewQuizDetailActivity;

    private RecyclerView recyclerViewSampleQuestionsList;
    private QuestionPreviewAdapter questionPreviewAdapter;
    private RecyclerView.LayoutManager layoutManager;

    private  Button btnPlayInViewQuizDetail;

    private String jwtToken, username;
    private ArrayList<QuestionPreview> questionPreviewsList;
    private ArrayList<Question> questionsList;

    private Quiz quiz;
    //-------------------------------------------------------------------------------
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_quiz_detail);

        //Get quiz data from previous activity/fragment
        Intent intent = getIntent();
        quiz = (Quiz) intent.getSerializableExtra("quiz");
        System.out.println(quiz.toString());

        //Initialize loading dialog
        progressDialog = new ProgressDialog(this);

        //Initialize all widgets ids
        ivQuizImageInViewQuizDetail = findViewById(R.id.ivQuizImageInViewQuizDetail);
        tvNumberOfQuestionsInViewQuizDetail = findViewById(R.id.tvNumberOfQuestionsInViewQuizDetail);
        tvQuizNameInViewQuizDetail = findViewById(R.id.tvQuizNameInViewQuizDetail);
        tvCategoryInViewQuizDetail = findViewById(R.id.tvCategoryInViewQuizDetail);
        tvQuizAuthorInViewQuizDetail = findViewById(R.id.tvQuizAuthorInViewQuizDetail);
        tvThisQuizCurrentlyHasNoQuestion = findViewById(R.id.tvThisQuizCurrentlyHasNoQuestion);

        btnBackFromViewQuizDetailActivity = findViewById(R.id.btnBackFromViewQuizDetailActivity);
        btnPlayInViewQuizDetail = findViewById(R.id.btnPlayInViewQuizDetail);

        recyclerViewSampleQuestionsList = findViewById(R.id.recyclerViewSampleQuestionsList);

        //Bind quiz information to widgets
        if (quiz.getAvatar() != null && !quiz.getAvatar().equals("NONE")){
            Glide.with(ViewQuizDetailActivity.this)
                    .load(quiz.getAvatar())
                    .centerCrop()
                    .into(ivQuizImageInViewQuizDetail);

            ivQuizImageInViewQuizDetail.setPadding(0, 0,0,0);
        }

        String textNumberOfQuestion = "question";
        if (quiz.getQuestion() != null) {
            if (quiz.getQuestion().size() > 1){
                textNumberOfQuestion = textNumberOfQuestion + "s";
            }

            textNumberOfQuestion = quiz.getQuestion().size() + " " + textNumberOfQuestion;
            tvNumberOfQuestionsInViewQuizDetail.setText(textNumberOfQuestion);
        }

        tvQuizNameInViewQuizDetail.setText(quiz.getName());

        String textCategory = "Category: " + quiz.getCategory();
        tvCategoryInViewQuizDetail.setText(textCategory);

        tvQuizAuthorInViewQuizDetail.setText(quiz.getAuthor());

        //Set behaviour for button back
        btnBackFromViewQuizDetailActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewQuizDetailActivity.super.onBackPressed();
            }
        });

        //Set up recyclerview sample questions
        questionPreviewsList = new ArrayList<>();
        layoutManager = new LinearLayoutManager(this);
        questionPreviewAdapter = new QuestionPreviewAdapter(questionPreviewsList);

        recyclerViewSampleQuestionsList.setLayoutManager(layoutManager);
        recyclerViewSampleQuestionsList.setAdapter(questionPreviewAdapter);

        //Set behaviour for button play
        btnPlayInViewQuizDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent(ViewQuizDetailActivity.this, ScoreboardOfGameplayActivity.class);
                intent1.putExtra("questionsList", (Serializable) questionsList);
                intent1.putExtra("currentPosition", "0");
                intent1.putExtra("currentScore", "0");

                startActivity(intent1);
            }
        });

        //Get JWT token to access APIs
        getJWTToken();

        //Call API to get all questions of this quiz
        questionsList = new ArrayList<>();
        getAllQuestionsOfQuiz(quiz.get_id());
    }
    //-------------------------------------------------------------------------------
    private void showProgressDialog(String text) {
        progressDialog.setTitle(text);
        progressDialog.show();
    }

    //-------------------------------------------------------------------------------
    private void getJWTToken() {
        SharedPreferences loginPref = getSharedPreferences("Login", Context.MODE_PRIVATE);
        username = loginPref.getString("loggedInUser", null);
        jwtToken = loginPref.getString("accessToken", null);
    }

    private void getAllQuestionsOfQuiz(String quizId){
        showProgressDialog("Loading data..");

        //Initialize needed things to call API
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .addInterceptor(loggingInterceptor)
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://backend-lms.herokuapp.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .build();

        APIService apiService = retrofit.create(APIService.class);

        //Call API to get all questions of this quiz
        Call<List<Question>> call = apiService.getAllQuestionOfQuiz(jwtToken, quizId);

        call.enqueue(new Callback<List<Question>>() {
            @Override
            public void onResponse(Call<List<Question>> call, Response<List<Question>> response) {
                if (progressDialog.isShowing()){
                    progressDialog.dismiss();
                }

                if (!response.isSuccessful()) {
                    System.out.println("code: " + response.code());
                    return;
                }

                List<Question> responseQuestionsList = response.body();
                System.out.println("**************************************");
                System.out.println(responseQuestionsList.toString());

                questionsList.addAll(responseQuestionsList);

                if (responseQuestionsList.size() < 1) {
                    tvThisQuizCurrentlyHasNoQuestion.setVisibility(View.VISIBLE);
                } else {
                    tvThisQuizCurrentlyHasNoQuestion.setVisibility(View.INVISIBLE);
                }

                for (Question question: responseQuestionsList){
                    questionPreviewsList.add(
                            new QuestionPreview(question.get_id(),
                                    question.getType(),
                                    question.getImage(),
                                    question.getQuestion()));

                    questionPreviewAdapter.notifyItemInserted(questionPreviewsList.size() - 1);
                }
            }

            @Override
            public void onFailure(Call<List<Question>> call, Throwable t) {
                if (progressDialog.isShowing()){
                    progressDialog.dismiss();
                }

                Toast.makeText(ViewQuizDetailActivity.this, "No response received from API", Toast.LENGTH_SHORT).show();
            }
        });
    }
}