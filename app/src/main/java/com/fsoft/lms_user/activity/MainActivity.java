package com.fsoft.lms_user.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.annotation.MenuRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.view.menu.MenuBuilder;
import androidx.appcompat.view.menu.MenuPopupHelper;
import androidx.appcompat.widget.PopupMenu;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.fsoft.lms_user.R;
import com.fsoft.lms_user.callback.MainCallbacks;
import com.fsoft.lms_user.component.CuteSnackbar;
import com.fsoft.lms_user.fragment.ActivityFragment;
import com.fsoft.lms_user.fragment.HomeFragment;
import com.fsoft.lms_user.fragment.SearchFragment;
import com.fsoft.lms_user.fragment.SettingsFragment;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

@SuppressWarnings("FieldCanBeLocal")
public class MainActivity extends AppCompatActivity implements MainCallbacks {

    // GUI elements
    private RelativeLayout main_activity;
    private Toolbar toolbar;
    private BottomNavigationView navbar;
    private Button action_login;
    private RelativeLayout action_profile;
    private ImageView action_profile_imageView;
    private ImageButton btnDashboard;

    // Snackbar
    private FrameLayout fragment_container;
    private CuteSnackbar snackbar;

    // Fragment data
    private HomeFragment home_frag;
    private SearchFragment search_frag;
    private ActivityFragment activity_frag;
    private SettingsFragment settings_frag;
    private String current_frag;

    // Request codes
    public static final int LOGIN_REQUEST_CODE = 706;
    public static final int SIGNUP_REQUEST_CODE = 516;
    public static final int SIGNUP_PROFILE_REQUEST_CODE = 564;
    public static final int WEBVIEW_REQUEST_CODE = 338;
    public static final int GOOGLE_REQUEST_CODE = 566;
    public static final int FACEBOOK_REQUEST_CODE = 538;

    // API data
    public static final String API_URL = "https://backend-lms.herokuapp.com";
    public static String loggedInUser, accessToken;
    public static Bundle userData;
    public static SharedPreferences loginPref;
    public static final String LOGIN_URL = API_URL + "/user/login",
                                SIGNUP_URL = API_URL + "/user/register",
                                PROFILE_URL = API_URL + "/api/manage-profile/%s",
                                QUESTION_URL = API_URL + "/api/question/manage-question",
                                QUIZ_URL = API_URL + "/api/library/quiz";
    private RequestQueue requestQueue;
    private int failFlag = 3;
    public static GoogleSignInOptions GSO;

    // Debugging
    private static final String TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Hide view (will show after having updated all the UI)
        main_activity = findViewById(R.id.main_activity);
        main_activity.setVisibility(View.GONE);

        // Set up ActivitySwitcher
        ActivitySwitcher activitySwitcher = new ActivitySwitcher(this, "main");

        // Set up toolbar
        toolbar = findViewById(R.id.toolbar);
        toolbar.inflateMenu(R.menu.menu_home);
        action_login = findViewById(R.id.action_login);
        action_login.setOnClickListener(view -> activitySwitcher.startLogin(current_frag));
        action_profile = findViewById(R.id.action_profile);
        action_profile_imageView = findViewById(R.id.action_profile_imageView);

        // Set up snackbar
        fragment_container = findViewById(R.id.fragment_container);
        snackbar = new CuteSnackbar(this, fragment_container);

        // Set up navigation bar
        navbar = findViewById(R.id.navbar);
        navbar.setItemIconTintList(null);
        navbar.setOnItemSelectedListener(this::onNavigationItemSelected);

        // Set up Dashboard button
        btnDashboard = findViewById(R.id.btnDashboard);
        btnDashboard.setVisibility(View.GONE);

        // Allocate needed fragments
        home_frag = HomeFragment.newInstance();
        search_frag = SearchFragment.newInstance();
        activity_frag = ActivityFragment.newInstance();
        settings_frag = SettingsFragment.newInstance();
        switchFragment("home");

        // Set up API
        requestQueue = Volley.newRequestQueue(this);
        GSO = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.firebase_web_client_id))
                .requestEmail()
                .build();

        // Get SharedPreferences
        loginPref = getSharedPreferences("Login", MODE_PRIVATE);

        // Process data from API
        checkLogin();

        //Set up dashboard button
        btnDashboard = findViewById(R.id.btnDashboard);
        btnDashboard.setOnClickListener(v -> {
            Intent intent = new Intent(MainActivity.this, DashboardActivity.class);
            startActivity(intent);
        });

        //Set up demo gameplay button
//        ImageView btnDemoGameplay = findViewById(R.id.btnDemoGameplay);
//        btnDemoGameplay.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(MainActivity.this, ScoreboardOfGameplayActivity.class);
//                startActivity(intent);
//            }
//        });
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (requestQueue != null) {
            requestQueue.cancelAll(TAG);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        // Process data from API
        checkLogin();
    }

    @SuppressLint("NonConstantResourceId")
    private boolean onNavigationItemSelected(MenuItem menuItem) {
        switchFragment(menuItem.getTitle().toString().toLowerCase(Locale.ROOT));
        return true;
    }

    public boolean onMenuItemClick(@NonNull MenuItem item) {
        super.onContextItemSelected(item);
        String title = item.getTitle().toString();
        if (title.equals(getString(R.string.action_profile_settings))) {
            switchFragment("settings");
        } else if (title.equals(getString(R.string.action_profile_library))) {
            // TODO: Switch to Library
        } else if (title.equals(getString(R.string.action_profile_logout))) {
            logout();
        } else {
            Log.w(TAG, "onContextItemSelected received unknown context item: " + title);
        }
        return true;
    }

    @SuppressLint("RestrictedApi")
    private MenuPopupHelper initPopupMenuIcon(View view, @MenuRes int menu, @Nullable String headerText) {
        // Create a popup menu
        PopupMenu popupMenu = new PopupMenu(this, view);
        popupMenu.inflate(menu);
        popupMenu.getMenu().setGroupDividerEnabled(true);
        popupMenu.setOnMenuItemClickListener(this::onMenuItemClick);

        // Set profile picture
        Picasso.get().setLoggingEnabled(true);
        Picasso.get()
                .load(userData.getString("avatar").replace("\\", ""))
                .fit()
                .centerCrop()
                .placeholder(R.drawable.info_profile)
                .into(action_profile_imageView);

        // Set special title
        if (headerText != null) {
            popupMenu.getMenu().getItem(0)
                    .setTitle(
                            Html.fromHtml(
                                    "<font color=\"#FFC84F\"><i><b>" +
                                            headerText +
                                            "</b></i></font>",
                                    Html.FROM_HTML_MODE_LEGACY
                            )
                    );
        }

        // Check account type (lecturer/student)

        // Add icons to each item
        Context popupContext = new ContextThemeWrapper(this, R.style.Theme_lms_user_PopupMenu_Icons);
        MenuPopupHelper menuPopupHelper = new MenuPopupHelper(popupContext, (MenuBuilder) popupMenu.getMenu(), view);
        menuPopupHelper.setForceShowIcon(true);
        menuPopupHelper.setGravity(Gravity.END);
        return menuPopupHelper;
    }

    private void switchFragment(Fragment fragment) {
        if (fragment != null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragment_container, fragment)
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                    .commit();
        }
    }

    private void logout() {
        loggedInUser = "";
        loginPref.edit().putString("loggedInUser","").apply();
        accessToken = "";
        loginPref.edit().putString("accessToken","").apply();
        finish();
        overridePendingTransition(0, 0);
        startActivity(getIntent());
        overridePendingTransition(0, 0);
    }

    @SuppressLint("RestrictedApi")
    private void checkLogin() {
        loggedInUser = loginPref.getString("loggedInUser", null);
        accessToken = loginPref.getString("accessToken", null);
        if (TextUtils.isEmpty(loggedInUser)) {
            action_login.setVisibility(View.VISIBLE);
            action_profile.setVisibility(View.GONE);

            // Show the UI
            main_activity.setVisibility(View.VISIBLE);
            return;
        }

        // Get user data
        userData = new Bundle();
        userData.putString("username", loggedInUser);
        JsonObjectRequest jsonRequest = new JsonObjectRequest(
                Request.Method.GET,
                String.format(Locale.ROOT, PROFILE_URL, loggedInUser),
                null,
                response -> {
                    Log.i(TAG, "Profile responded with json: " + response.toString());
                    try {
                        if (response.has("message")) {
                            throw new Throwable(response.getString("message"));
                        }
                        userData.putString("avatar", response.getString("avatar"));
                        userData.putString("name", response.getString("name"));
                        userData.putString("email", response.getString("email").replace("\\/", "/"));
                        userData.putString("type", response.getString("type"));
                        userData.putString("birthyear", response.getString("birthyear"));

                        // Update the UI to match user data from API
                        String name = userData.getString("name");
                        if (TextUtils.isEmpty(name)) {
                            name = loggedInUser;
                        }
                        action_login.setVisibility(View.GONE);
                        action_profile.setVisibility(View.VISIBLE);
                        MenuPopupHelper menu = initPopupMenuIcon(action_profile, R.menu.menu_profile,
                                getString(R.string.info_logged_in, name));
                        action_profile.setOnClickListener(view -> menu.show());
                        if (userData.getString("type").equals("true")) {
                            btnDashboard.setVisibility(View.VISIBLE);
                        }
                        Bundle bundle = new Bundle();
                        bundle.putString("action", "refresh");
                        try {
                            home_frag.mainToFrag(bundle);
                        } catch (Exception ignored) {}

                        // Show the UI
                        main_activity.setVisibility(View.VISIBLE);
                    } catch (Throwable e) {
                        Log.e(TAG, "Profile met with an error: " + e.getMessage());
                        snackbar.setContent(R.drawable.exclamation, R.string.info_profile_get_failed)
                                .show();
                        logout();
                    }
                }, error -> {
                    try {
                        String body = "null",
                                statusCode = String.valueOf(error.networkResponse.statusCode);
                        Log.e(TAG, "Profile response produced error: " + statusCode + " - " + body);
                    } catch (Exception e) {
                        Log.e(TAG, "Profile response produced error: " + error.getMessage());
                    }
                    if (failFlag > 0) {
                        failFlag--;
                        checkLogin();
                    } else {
                        snackbar.setContent(R.drawable.exclamation, R.string.info_profile_get_failed);
                        logout();
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() {
                return new HashMap<String, String>() {
                    {
                        put("access-token", accessToken);
                    }
                };
            }
        };

        jsonRequest.setTag(TAG);
        requestQueue.add(jsonRequest);
    }

    private void switchFragment(String fragName) {
        Fragment selected_fragment;
        int drawableId;
        switch (fragName) {
            case "home":
                selected_fragment = home_frag;
                drawableId = R.drawable.navbar_home;
                navbar.getMenu().findItem(R.id.navbar_home).setChecked(true);
                break;
            case "search":
                selected_fragment = search_frag;
                drawableId = R.drawable.navbar_search;
                navbar.getMenu().findItem(R.id.navbar_search).setChecked(true);
                break;
            case "activity":
                selected_fragment = activity_frag;
                drawableId = R.drawable.navbar_activity;
                navbar.getMenu().findItem(R.id.navbar_activity).setChecked(true);
                break;
            case "settings":
                selected_fragment = settings_frag;
                drawableId = R.drawable.navbar_settings;
                navbar.getMenu().findItem(R.id.navbar_settings).setChecked(true);
                break;
            default:
                Log.e(TAG, "switchFragment received unknown fragment name: " + fragName);
                return;
        }
        switchFragment(selected_fragment);
        current_frag = fragName;
        toolbar.setTitle(fragName.substring(0, 1).toUpperCase() + fragName.substring(1));
        toolbar.setNavigationIcon(drawableId);
    }

    @Override
    public void fragToMain(String caller, Bundle bundle) {
        // Check action sent
        if (bundle.getString("action") != null) {
            String action = bundle.getString("action");
            if (action.equals("switch")) {
                switchFragment(bundle.getString("caller"));
            } else {
                Log.w(TAG, "fragToMain received unknown action request: " + action);
            }
        }
    }

}