package com.fsoft.lms_user.activity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.fsoft.lms_user.APIService;
import com.fsoft.lms_user.R;
import com.fsoft.lms_user.adapter.QuestionPreviewAdapter;
import com.fsoft.lms_user.adapter.SpinnerScopeAdapter;
import com.fsoft.lms_user.model.Category;
import com.fsoft.lms_user.model.QuestionPreview;
import com.fsoft.lms_user.model.Quiz;
import com.fsoft.lms_user.model.ScopeItem;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@SuppressWarnings("FieldCanBeLocal")
public class CreateQuizActivity extends AppCompatActivity {
    public static final int PICK_IMAGE_REQUEST = 1;
    public static final int GET_CREATED_QUESTION_REQUEST = 2;

    Uri imageUri;
    private StorageReference storageReference;

    private ImageView btnBackFromActivityCreateQuiz, btnCreateQuiz, coverImage;
    private LinearLayout btnAddCoverImage;
    private ImageButton btnAddQuestion;
    private EditText edtQuizName;
    private ProgressDialog progressDialog;
    private AutoCompleteTextView autoCompleteQuizCategory;

    private RecyclerView recyclerViewQuestionPreviewsList;
    private QuestionPreviewAdapter questionPreviewAdapter;
    private RecyclerView.LayoutManager layoutManager;

    private Spinner spinnerScopes;
    private ArrayList<ScopeItem> scopeItems;
    private SpinnerScopeAdapter spinnerScopeAdapter;

    private String coverImageURL, quizName, quizCategory, quizScope,username, jwtToken;
    private List<String> categoriesList;
    ArrayAdapter<String> autoCompleteAdapter;
    ArrayList<QuestionPreview> questionPreviewsList;
    ArrayList<String> quizQuestions;

    Quiz quiz;

    //----------------------------------------------------------------------------------------------
    HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY);

    OkHttpClient okHttpClient = new OkHttpClient.Builder()
            .addInterceptor(loggingInterceptor)
            .build();

    Retrofit retrofit = new Retrofit.Builder()
            .baseUrl("https://backend-lms.herokuapp.com/")
            .addConverterFactory(GsonConverterFactory.create())
            .client(okHttpClient)
            .build();

    APIService apiService = retrofit.create(APIService.class);
    //----------------------------------------------------------------------------------------------
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_quiz);

        progressDialog = new ProgressDialog(this);

        //Set up all widgets ids
        btnBackFromActivityCreateQuiz = findViewById(R.id.btnBackFromActivityCreateQuiz);
        btnCreateQuiz = findViewById(R.id.btnCreateQuiz);
        coverImage = findViewById(R.id.coverImage);
        btnAddCoverImage = findViewById(R.id.btnAddCoverImage);
        edtQuizName = findViewById(R.id.edtQuizName);
        spinnerScopes = findViewById(R.id.spinnerScopes);
        btnAddQuestion = findViewById(R.id.btnAddQuestion);
        autoCompleteQuizCategory = findViewById(R.id.autoCompleteQuizCategory);
        recyclerViewQuestionPreviewsList = findViewById(R.id.recyclerViewQuestionPreviewsList);

        // Set up auto complete quiz category
        categoriesList = new ArrayList<>();
        autoCompleteAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, categoriesList);
        autoCompleteQuizCategory.setAdapter(autoCompleteAdapter);

        //Initialize needed things
        quizQuestions = new ArrayList<>();
        getJWTToken();
        getAllCategories();

        // Set up button Back
        btnBackFromActivityCreateQuiz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CreateQuizActivity.super.onBackPressed();
            }
        });

        // Set up button Create Quiz
        btnCreateQuiz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edtQuizName.getText().toString().length() < 1) {
                    Toast.makeText(CreateQuizActivity.this, "Please fill up quiz name", Toast.LENGTH_SHORT).show();
                } else {
                    if (autoCompleteQuizCategory.getText().toString().length() < 1){
                        Toast.makeText(CreateQuizActivity.this, "Please fill up quiz category", Toast.LENGTH_SHORT).show();
                    } else {
                        createQuiz();
                    }
                }
            }
        });

        // Set up button add cover image
        btnAddCoverImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectThenUploadImage();
            }
        });

        // Set up list for spinner
        initScopeList();

        //Set up spinner
        spinnerScopeAdapter = new SpinnerScopeAdapter(this, scopeItems);
        spinnerScopes.setAdapter(spinnerScopeAdapter);

        // Set up spinner behaviour
        spinnerScopes.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ScopeItem clickedItem = (ScopeItem) parent.getItemAtPosition(position);
                String scopeName = clickedItem.getScopeName();
                quizScope = scopeName.split(",")[0];
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        //Set up recycler view
        questionPreviewsList = new ArrayList<>();
        layoutManager = new LinearLayoutManager(this);
        questionPreviewAdapter = new QuestionPreviewAdapter(questionPreviewsList);

        recyclerViewQuestionPreviewsList.setLayoutManager(layoutManager);
        recyclerViewQuestionPreviewsList.setAdapter(questionPreviewAdapter);

        // Set up add question button
        btnAddQuestion.setOnClickListener(view -> {
            Intent intent = new Intent(CreateQuizActivity.this, AddQuestionActivity.class);
            startActivityForResult(intent, GET_CREATED_QUESTION_REQUEST);
        });
    }

    //-------------------------------------------------------------------------------
    private void showProgressDialog(String text) {
        progressDialog.setTitle(text);
        progressDialog.show();
    }

    //-------------------------------------------------------------------------------
    private void getJWTToken() {
        SharedPreferences loginPref = getSharedPreferences("Login", MODE_PRIVATE);
        username = loginPref.getString("loggedInUser", null);
        jwtToken = loginPref.getString("accessToken", null);
    }

    private void getAllCategories() {
        showProgressDialog("Loading data..");

        //Call API to get data
        Call<List<Category>> call = apiService.getAllCategories();

        call.enqueue(new Callback<List<Category>>() {
            @Override
            public void onResponse(Call<List<Category>> call, Response<List<Category>> response) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                if (!response.isSuccessful()) {
                    Toast.makeText(CreateQuizActivity.this, "Load data failed", Toast.LENGTH_SHORT).show();
                    System.out.println("Code: " + response.code());
                    return;
                }

                List<Category> responseList = response.body();
                for (Category category : responseList)
                    categoriesList.add(category.getCategoryName());

                autoCompleteAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<List<Category>> call, Throwable t) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                Toast.makeText(CreateQuizActivity.this, "No response received from API", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void selectThenUploadImage() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, PICK_IMAGE_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && data != null && data.getData() != null) {
            imageUri = data.getData();
            btnAddCoverImage.setPadding(0, 0, 0, 0);
            coverImage.setScaleType(ImageView.ScaleType.CENTER_CROP);
            coverImage.setImageURI(imageUri);

            uploadImage();
        }

        if (requestCode == GET_CREATED_QUESTION_REQUEST && resultCode == RESULT_OK){
            Bundle bundle = data.getExtras();

            String id, type, imageURL, question;
            id = bundle.getString("id");
            type = bundle.getString("type");
            imageURL = bundle.getString("image");
            question = bundle.getString("question");

            QuestionPreview questionPreview = new QuestionPreview(id, type, imageURL, question);
            System.out.println(questionPreview.toString());

            questionPreviewsList.add(questionPreview);
            questionPreviewAdapter.notifyItemInserted(questionPreviewsList.size() - 1);

            quizQuestions.add(questionPreview.get_id());
        }
    }

    private void uploadImage() {
        showProgressDialog("Uploading image..");

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss", Locale.CHINA);
        Date now = new Date();
        String fileName = username + "_" + simpleDateFormat.format(now);

        // Upload selected cover image to firebase storage
        storageReference = FirebaseStorage.getInstance().getReference("images/" + fileName);
        storageReference.putFile(imageUri)
                .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        if (progressDialog.isShowing()) {
                            progressDialog.dismiss();
                        }
                        Toast.makeText(CreateQuizActivity.this, "Upload image successfully", Toast.LENGTH_SHORT).show();

                        // Get image url after upload image to firebase storage
                        storageReference.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                            @Override
                            public void onSuccess(Uri uri) {
                                coverImageURL = uri.toString();
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                System.out.println("Failed to get image URL");
                            }
                        });
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        if (progressDialog.isShowing()) {
                            progressDialog.dismiss();
                        }
                        Toast.makeText(CreateQuizActivity.this, "Upload image failed", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void createQuiz(){
        showProgressDialog("Creating quiz..");

        //Initialize object Quiz
        if (coverImageURL == null){
            coverImageURL ="NONE";
        }
        quizName = edtQuizName.getText().toString();
        quizCategory = autoCompleteQuizCategory.getText().toString();

//        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss", Locale.CHINA);
//        Date now = new Date();
//        String currentTime = simpleDateFormat.format(now);

        quiz = new Quiz(null, quizName, quizCategory, coverImageURL, quizScope, username, null, quizQuestions);

        //Call API to post data
        Call<Quiz> call = apiService.postQuiz(jwtToken, quiz);

        call.enqueue(new Callback<Quiz>() {
            @Override
            public void onResponse(Call<Quiz> call, Response<Quiz> response) {
                if (progressDialog.isShowing()){
                    progressDialog.dismiss();
                }

                if (!response.isSuccessful()){
                    Toast.makeText(getApplicationContext(), "Create quiz failed", Toast.LENGTH_SHORT).show();
                    System.out.println("code: " + response.code());
                    return;
                }

                Toast.makeText(getApplicationContext(), "Create quiz successfully", Toast.LENGTH_SHORT).show();

                Quiz responseQuiz = response.body();
                System.out.println("**************************************");
                System.out.println("Response quiz: " + responseQuiz.toString());

                finish();
            }

            @Override
            public void onFailure(Call<Quiz> call, Throwable t) {
                if (progressDialog.isShowing()){
                    progressDialog.dismiss();
                }

                Toast.makeText(CreateQuizActivity.this, "No response received from API", Toast.LENGTH_SHORT).show();
            }
        });
    }

    //-------------------------------------------------------------------------------
    private void initScopeList() {
        scopeItems = new ArrayList<>();
        scopeItems.add(new ScopeItem("Public, visible to everyone", R.drawable.ic_visible));
        scopeItems.add(new ScopeItem("Private, visible to you", R.drawable.ic_invisible));
    }

}