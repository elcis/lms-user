package com.fsoft.lms_user.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.fsoft.lms_user.R;
import com.fsoft.lms_user.model.Question;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class TestGamePlayActivity extends AppCompatActivity {
    private ProgressBar countDownBar;

    private Button btnAnswerA, btnAnswerB, btnAnswerC, btnAnswerD;
    private Button btnCorrectAnswer;
    private Button btnNextInGameplay;
    private LinearLayout correctAnswerNotification, wrongAnswerNotification;

    private TextView tvScoreAchieved;
    private TextView tvWrongAnswerNotification;
    private TextView tvQuestionContentInGameplay;
    private TextView tvCurrentQuestionInGameplay;

    private ImageView btnBackFromActivityGameplay;
    private ImageView ivQuestionImageInGameplay;

    private final Timer timer = new Timer();
    private int elapsedTimeInPercent = 0;
    private int i = 0;

    private String currentPosition;
    private ArrayList<Question> questionsList;
    private String currentScore;
    private Question currentQuestion;

    private int score;

    //------------------------------------------------------------------------------------------
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_game_play);

        //Get data from previous activity
        Intent intentGetData = getIntent();
        questionsList = (ArrayList<Question>) intentGetData.getSerializableExtra("questionsList");
        System.out.println(questionsList.toString());
        currentPosition = intentGetData.getStringExtra("currentPosition");
        currentScore = intentGetData.getStringExtra("currentScore");

        int position = Integer.parseInt(currentPosition);
        if (position > -1 && position < questionsList.size()){
            currentQuestion = questionsList.get(position);
        }

        //Set up all widgets ids
        ivQuestionImageInGameplay = findViewById(R.id.ivQuestionImageInGameplay);
        tvQuestionContentInGameplay = findViewById(R.id.tvQuestionContentInGameplay);
        tvCurrentQuestionInGameplay = findViewById(R.id.tvCurrentQuestionInGameplay);

        countDownBar = findViewById(R.id.countDownBar);

        btnAnswerA = findViewById(R.id.btnAnswerA);
        btnAnswerB = findViewById(R.id.btnAnswerB);
        btnAnswerC = findViewById(R.id.btnAnswerC);
        btnAnswerD = findViewById(R.id.btnAnswerD);

        correctAnswerNotification = findViewById(R.id.correctAnswerNotification);
        tvScoreAchieved = findViewById(R.id.tvScoreAchieved);
        wrongAnswerNotification = findViewById(R.id.wrongAnswerNotification);
        tvWrongAnswerNotification = findViewById(R.id.tvWrongAnswerNotification);

        btnBackFromActivityGameplay = findViewById(R.id.btnBackFromActivityGameplay);
        btnNextInGameplay = findViewById(R.id.btnNextInGameplay);

        //Set behaviour for back button
        btnBackFromActivityGameplay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TestGamePlayActivity.super.onBackPressed();
            }
        });
        btnBackFromActivityGameplay.setVisibility(View.GONE);

        //--------------------------Set-up-question-contents-------------------------------------------------------------------
        //Set up current question count
        String textCurrentQuestionPosition = Integer.toString(position + 1) + "/" + Integer.toString(questionsList.size());
        tvCurrentQuestionInGameplay.setText(textCurrentQuestionPosition);

        //Set up cover image of question
        if (currentQuestion.getImage() != null && !currentQuestion.getImage().equals("NONE")){
            Glide.with(this)
                    .load(currentQuestion.getImage())
                    .centerCrop()
                    .into(ivQuestionImageInGameplay);

            ivQuestionImageInGameplay.setPadding(0,0,0,0);
        } else {
            ivQuestionImageInGameplay.setImageResource(R.drawable.ic_picture);
            int padding = getResources().getDimensionPixelSize(R.dimen.add_cover_image_padding);
            ivQuestionImageInGameplay.setPadding(padding,padding,padding,padding);
        }

        //Set up question
        tvQuestionContentInGameplay.setText(currentQuestion.getQuestion());

        //Set up answer buttons
        btnAnswerA.setText(currentQuestion.getAnswer().get(0));
        btnAnswerB.setText(currentQuestion.getAnswer().get(1));
        btnAnswerC.setText(currentQuestion.getAnswer().get(2));
        btnAnswerD.setText(currentQuestion.getAnswer().get(3));

        //-------------------------Set-behaviour-for-answer-buttons------------------------------------------------------------
        String answerA = (String) btnAnswerA.getText();
        String answerB = (String) btnAnswerB.getText();
        String answerC = (String) btnAnswerC.getText();

        String strCorrectPosition = currentQuestion.getCorrect_answer().get(0);
        int correctPosition = Integer.parseInt(strCorrectPosition);

        String correctAnswer = currentQuestion.getAnswer().get(correctPosition);

        if (correctAnswer.equals(answerA))
            btnCorrectAnswer = btnAnswerA;
        else if (correctAnswer.equals(answerB))
            btnCorrectAnswer = btnAnswerB;
        else if (correctAnswer.equals(answerC))
            btnCorrectAnswer = btnAnswerC;
        else
            btnCorrectAnswer = btnAnswerD;

        btnAnswerA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                processAnswer(btnAnswerA);
            }
        });

        btnAnswerB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                processAnswer(btnAnswerB);
            }
        });

        btnAnswerC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                processAnswer(btnAnswerC);
            }
        });

        btnAnswerD.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                processAnswer(btnAnswerD);
            }
        });

        //-------------------------------------Count-down-bar---------------------------------------------------
        int questionTime = Integer.parseInt(currentQuestion.getTime()) * 1000;
        int taskRepeatTime = 100;

        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                i++;
                elapsedTimeInPercent = (i * taskRepeatTime) * 100 / questionTime;
                countDownBar.setProgress(100 - elapsedTimeInPercent);

                if (elapsedTimeInPercent == 100) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            btnAnswerA.setClickable(false);
                            btnAnswerB.setClickable(false);
                            btnAnswerC.setClickable(false);
                            btnAnswerD.setClickable(false);

                            btnAnswerA.setBackgroundColor(Color.parseColor("#FF98A9"));
                            btnAnswerB.setBackgroundColor(Color.parseColor("#FF98A9"));
                            btnAnswerC.setBackgroundColor(Color.parseColor("#FF98A9"));
                            btnAnswerD.setBackgroundColor(Color.parseColor("#FF98A9"));
                            btnCorrectAnswer.setBackgroundColor(Color.parseColor("#66bE3D"));

                            wrongAnswerNotification.setVisibility(View.VISIBLE);
                            tvWrongAnswerNotification.setText("Oops! Time-out");

                            countDownBar.setVisibility(View.INVISIBLE);
                            btnNextInGameplay.setVisibility(View.VISIBLE);
                        }
                    });

                    timer.cancel();
                }
            }
        };

        timer.schedule(timerTask, 0, 100);
        //-----------------------------------Set-behaviour-for-button-next---------------------------------------------------
        btnNextInGameplay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TestGamePlayActivity.this, ScoreboardOfGameplayActivity.class);

                intent.putExtra("questionsList", questionsList);

                int previousScore = Integer.parseInt(currentScore);
                score = score + previousScore;
                String strScore = Integer.toString(score);
                intent.putExtra("currentScore", strScore);

                String strPosition = Integer.toString(position + 1);
                intent.putExtra("currentPosition", strPosition);

                startActivity(intent);
            }
        });

    }

    //------------------------------------------------------------------------------------------
    private void processAnswer(Button btnAnswer) {
        btnAnswerA.setClickable(false);
        btnAnswerB.setClickable(false);
        btnAnswerC.setClickable(false);
        btnAnswerD.setClickable(false);

        btnAnswerA.setBackgroundColor(Color.parseColor("#FF98A9"));
        btnAnswerB.setBackgroundColor(Color.parseColor("#FF98A9"));
        btnAnswerC.setBackgroundColor(Color.parseColor("#FF98A9"));
        btnAnswerD.setBackgroundColor(Color.parseColor("#FF98A9"));

        if (!btnAnswer.equals(btnCorrectAnswer)) {
            btnAnswer.setBackgroundColor(Color.parseColor("#FF3356"));
            wrongAnswerNotification.setVisibility(View.VISIBLE);
        } else {
            correctAnswerNotification.setVisibility(View.VISIBLE);

            score = 1000 + ((100 - elapsedTimeInPercent) * 1000) / 100;
            String scoreText = "+" + Integer.toString(score);
            tvScoreAchieved.setText(scoreText);
        }

        btnCorrectAnswer.setBackgroundColor(Color.parseColor("#66bE3D"));

        timer.cancel();
        countDownBar.setVisibility(View.INVISIBLE);
        btnNextInGameplay.setVisibility(View.VISIBLE);
    }
}