package com.fsoft.lms_user.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.fsoft.lms_user.R;

@SuppressWarnings("FieldCanBeLocal")
public class DashboardActivity extends AppCompatActivity {
    private DrawerLayout drawerLayout;

    private Toolbar toolbarDashboard;
    private ImageView btnBackFromActivityDashboard;
    private ImageView btnHamburger;

    private ImageButton btnAddQuiz;


    //----------------------------------------------------------------------
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        //Set up toolbar
        toolbarDashboard = findViewById(R.id.toolbarDashboard);

        //Set up drawer layout and sync it with the toolbar
        drawerLayout = findViewById(R.id.drawerLayout);

        //Set up hamburger button
        btnHamburger = findViewById(R.id.btnHamburger);
        btnHamburger.setOnClickListener(v -> drawerLayout.openDrawer(GravityCompat.END));

        //Set up back button
        btnBackFromActivityDashboard = findViewById(R.id.btnBackFromActivityDashboard);
        btnBackFromActivityDashboard.setOnClickListener(v -> DashboardActivity.super.onBackPressed());

        btnAddQuiz = findViewById(R.id.btnAddQuiz);
        btnAddQuiz.setOnClickListener(v -> {
            Intent intent = new Intent(DashboardActivity.this, CreateQuizActivity.class);
            startActivity(intent);
        });
    }

    //-------------When click back btn of the phone, close the drawer-----------------
    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.END)) {
            drawerLayout.closeDrawer(GravityCompat.END);
        } else {
            super.onBackPressed();
        }
    }
}