package com.fsoft.lms_user.callback;

import android.os.Bundle;

public interface MainCallbacks {
    void fragToMain(String caller, Bundle bundle);
}