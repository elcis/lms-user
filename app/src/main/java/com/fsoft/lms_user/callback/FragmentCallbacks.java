package com.fsoft.lms_user.callback;

import android.os.Bundle;

public interface FragmentCallbacks {
    void mainToFrag(Bundle bundle);
}