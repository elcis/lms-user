package com.fsoft.lms_user;

import com.fsoft.lms_user.model.Category;
import com.fsoft.lms_user.model.Password;
import com.fsoft.lms_user.model.Question;
import com.fsoft.lms_user.model.Quiz;
import com.fsoft.lms_user.model.Search;
import com.fsoft.lms_user.model.User;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface APIService {
    //-----------------------------Settings----------------------------------------
    @GET("api/manage-profile/{username}")
    Call<User> getUser(@Header("access-token") String jwtToken,
                       @Path("username") String username);

    @PATCH("api/manage-profile/{username}")
    Call<User> patchUser(@Header("access-token") String jwtToken,
                         @Path("username") String username,
                         @Body User user);

    @PATCH("api/manage-profile/change/password")
    Call<Password> patchPassword(@Header("access-token") String jwtToken,
                                 @Body Password password);

    //---------------------------Create-Quiz------------------------------------------
    @GET("category/all")
    Call<List<Category>> getAllCategories();

    @POST("api/quiz/manage-quiz")
    Call<Quiz> postQuiz(@Header("access-token") String jwtToken,
                        @Body Quiz quiz);

    //------------------------------Gameplay----------------------------------------------
    @GET("api/question/manage-question/all/{quizId}")
    Call<List<Question>> getAllQuestionOfQuiz(@Header("access-token") String jwtToken,
                                              @Path("quizId") String quizId);

    @GET("api/question/manage-question/{questionId}")
    Call<Question> getQuestion(@Header("access-token") String jwtToken,
                               @Path("questionId") String questionId);

    //--------------------------------Search-quiz----------------------------------------------
//    @Query("name") String quizName
    @POST("api/library/search")
    Call<List<Quiz>> searchQuizByName(@Header("access-token") String jwtToken,
                                      @Body Search search);
}
