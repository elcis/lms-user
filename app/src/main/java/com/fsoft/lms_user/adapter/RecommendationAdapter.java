package com.fsoft.lms_user.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.fsoft.lms_user.R;
import com.fsoft.lms_user.activity.MainActivity;
import com.fsoft.lms_user.activity.ViewQuizDetailActivity;
import com.fsoft.lms_user.model.Quiz;
import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Locale;

@SuppressWarnings("FieldCanBeLocal")
public class RecommendationAdapter extends RecyclerView.Adapter<RecommendationAdapter.ViewHolder> {
    private final Context context;
    private final ArrayList<Quiz> quizzes;

    public RecommendationAdapter(Context context, ArrayList<Quiz> quizzes) {
        this.context = context;
        this.quizzes = quizzes;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // Create a new view, which defines the UI of the list item
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_quiz, parent, false);
        return new RecommendationAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Quiz quiz = quizzes.get(position);
        String avatar = quiz.getAvatar(),
                name = quiz.getName(),
                author = quiz.getAuthor();
        int qCount = quiz.getQuestion().size();
        Picasso.get()
                .load(avatar)
                .fit()
                .centerInside()
                .placeholder(R.drawable.ic_launcher_foreground)
                .into(holder.imageView);
        holder.quizName.setText(name);
        holder.quizAuthor.setText(author);
        holder.quizCount.setText(String.format(Locale.ROOT, "%d Questions", qCount));
        holder.itemView.setOnClickListener(view -> {
            Intent intent = new Intent(context, ViewQuizDetailActivity.class);
            intent.putExtra("quiz", quiz);
            context.startActivity(intent);
        });
    }

    @Override
    public int getItemCount() {
        return quizzes.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public View itemView;
        public ImageView imageView, quizStatus;
        public TextView quizName, quizAuthor, quizCount;

        public ViewHolder(@NonNull @NotNull View itemView) {
            super(itemView);
            this.itemView = itemView;
            imageView = itemView.findViewById(R.id.imageView);
            quizName = itemView.findViewById(R.id.quizName);
            quizAuthor = itemView.findViewById(R.id.quizAuthor);
            quizStatus = itemView.findViewById(R.id.quizStatus);
            quizCount = itemView.findViewById(R.id.quizCount);
        }
    }
}