package com.fsoft.lms_user.adapter;

import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;

import androidx.annotation.NonNull;
import androidx.annotation.StringRes;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.recyclerview.widget.RecyclerView;

import com.fsoft.lms_user.R;
import com.fsoft.lms_user.abtract.TextChangedListener;
import com.fsoft.lms_user.activity.AddQuestionActivity;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Objects;

@SuppressWarnings("FieldCanBeLocal")
public class CheckboxAdapter extends RecyclerView.Adapter<CheckboxAdapter.ViewHolder> {
    private final Context context;
    private final HashSet<Integer> answerIds;
    private final ArrayList<String> answersOptions;
    private final RecyclerView answerListView;
    private final LinearLayout addAnswerLayout;

    final ArrayList<ViewHolder> holders = new ArrayList<>();

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private final TextInputLayout textInputAnswer;
        private final TextInputEditText editTextAnswer;
        private final CheckBox checkboxAnswer;

        public ViewHolder(View view) {
            super(view);
            // Define click listener for the ViewHolder's View
            textInputAnswer = view.findViewById(R.id.textInputAnswer);
            editTextAnswer = view.findViewById(R.id.editTextAnswer);
            checkboxAnswer = view.findViewById(R.id.checkboxAnswer);
        }

        public TextInputLayout getTextInputAnswer() {
            return textInputAnswer;
        }

        public TextInputEditText getEditTextAnswer() {
            return editTextAnswer;
        }

        public CheckBox getCheckboxAnswer() {
            return checkboxAnswer;
        }
    }

    public CheckboxAdapter(Context context, ArrayList<String> answersOptions,
                           @NonNull RecyclerView answerListView, @NonNull LinearLayout addAnswerLayout) {
        this.context = context;
        this.answersOptions = answersOptions;
        this.answerListView = answerListView;
        this.addAnswerLayout = addAnswerLayout;
        answerIds = new HashSet<>();
    }

    public void toggleAnswerId(int id, boolean checked) {
        if (checked) {
            answerIds.add(id);
        } else {
            answerIds.remove(id);
        }
        updateQuestion("correct_answer", getAnswerIdJson());
    }

    public void setAnswer(int id, Editable s) {
        answersOptions.set(id, s.toString());
        updateQuestion("answer", getAnswerJson());
    }

    private String getAnswerIdJson() {
        StringBuilder answer = new StringBuilder("[");
        for (Integer a : answerIds) {
            answer.append("\"")
                    .append(a)
                    .append("\",");
        }
        answer.setLength(answer.length() - 1);
        answer.append("]");
        return answer.toString();
    }

    private String getAnswerJson() {
        StringBuilder answer = new StringBuilder("[");
        for (String a : answersOptions) {
            answer.append("\"")
                    .append(a)
                    .append("\",");
        }
        answer.setLength(answer.length() - 1);
        answer.append("]");
        return answer.toString();
    }

    private void updateQuestion(String key, String val) {
        Bundle bundle = new Bundle();
        bundle.putString("key", key);
        bundle.putString("val", val);
        ((AddQuestionActivity) context).fragToMain("checkbox", bundle);
    }

    public void addAnswer() {
        if (answersOptions.size() >= 5) {
            return;
        }
        answersOptions.add("");
        if (answersOptions.size() >= 5) {
            addAnswerLayout.setVisibility(View.GONE);
        }
    }

    public void removeAnswer(int id) {
        answersOptions.remove(answersOptions.get(id));
        addAnswerLayout.setVisibility(View.VISIBLE);
    }

    public void checkAnswers() {
        for (ViewHolder holder : holders) {
            onFocusChange(holder.getTextInputAnswer(), holders.indexOf(holder), false, R.string.hint_answer);
        }
    }

    // Create new views (invoked by the layout manager)
    @NotNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        // Create a new view, which defines the UI of the list item
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.layout_answer_checkbox, viewGroup, false);
        ViewHolder holder = new ViewHolder(view);
        holders.add(holder);
        return holder;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {

        // Get element from your dataset at this position and replace the
        // contents of the view with that element
        int pos = viewHolder.getAdapterPosition() + 1;
        TextInputLayout text = viewHolder.getTextInputAnswer();
        TextInputEditText edit = viewHolder.getEditTextAnswer();
        CheckBox checkbox = viewHolder.getCheckboxAnswer();

        text.setHint(context.getString(R.string.hint_answer, pos));
        text.setEndIconOnClickListener(view -> removeAnswer(pos - 1));
        edit.setText(answersOptions.get(position));
        edit.setOnFocusChangeListener((v, focused)
                -> onFocusChange(text, position, focused, R.string.hint_answer));
        edit.addTextChangedListener(
                new TextChangedListener<TextInputEditText>(edit) {
                    @Override
                    public void onTextChanged(TextInputEditText target, Editable s) {
                        setAnswer(pos - 1, s);
                    }
                });
        checkbox.setChecked(answerIds.contains(position));
        checkbox.setOnClickListener((v) -> toggleAnswerId(pos - 1, checkbox.isChecked()));
        // if (pos <= 2) {
        text.setEndIconVisible(false);
        // }
    }

    private void onFocusChange(TextInputLayout inputLayout, int position, boolean focused, @StringRes int stringId) {
        if (!focused) {
            String str = Objects.requireNonNull(inputLayout.getEditText()).getText().toString();
            if (str.isEmpty()) {
                inputLayout.setHint(String.format(context.getString(stringId), position));
                inputLayout.setBackground(
                        AppCompatResources.getDrawable(
                                context,
                                R.drawable.rounded_input_negative
                        )
                );
            } else {
                inputLayout.setHint("");
                inputLayout.setBackground(
                        AppCompatResources.getDrawable(
                                context,
                                R.drawable.rounded_input
                        )
                );
            }
        }
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return answersOptions.size();
    }
}