package com.fsoft.lms_user.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RadioButton;

import androidx.annotation.NonNull;
import androidx.annotation.StringRes;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.recyclerview.widget.RecyclerView;

import com.fsoft.lms_user.R;
import com.fsoft.lms_user.abtract.TextChangedListener;
import com.fsoft.lms_user.activity.AddQuestionActivity;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Objects;

public class MultipleChoiceAdapter extends RecyclerView.Adapter<MultipleChoiceAdapter.ViewHolder> {
    private final Context context;
    private static int answerId;
    private final ArrayList<String> answersOptions;
    private final RecyclerView answerListView;
    private final LinearLayout addAnswerLayout;

    final ArrayList<ViewHolder> holders = new ArrayList<>();

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private final TextInputLayout textInputAnswer;
        private final TextInputEditText editTextAnswer;
        private final RadioButton radioButtonAnswer;

        @SuppressLint("ClickableViewAccessibility")
        public ViewHolder(View view) {
            super(view);
            // Define click listener for the ViewHolder's View
            textInputAnswer = view.findViewById(R.id.textInputAnswer);
            editTextAnswer = view.findViewById(R.id.editTextAnswer);
            radioButtonAnswer = view.findViewById(R.id.radioButtonAnswer);
        }

        public TextInputLayout getTextInputAnswer() {
            return textInputAnswer;
        }

        public TextInputEditText getEditTextAnswer() {
            return editTextAnswer;
        }

        public RadioButton getRadioButtonAnswer() {
            return radioButtonAnswer;
        }
    }

    public MultipleChoiceAdapter(Context context, ArrayList<String> answersOptions,
                                 @NonNull RecyclerView answerListView, @NonNull LinearLayout addAnswerLayout) {
        this.context = context;
        this.answersOptions = answersOptions;
        this.answerListView = answerListView;
        this.addAnswerLayout = addAnswerLayout;
        answerId = 0;
    }

    public void setAnswerId(int id) {
        holders.get(answerId).getRadioButtonAnswer().setChecked(false);
        answerId = id;
        updateQuestion("correct_answer", "[\"" + id + "\"]");
    }

    public void setAnswer(int id, Editable s) {
        answersOptions.set(id, s.toString());
        updateQuestion("answer", getAnswerJson());
    }

    private String getAnswerJson() {
        StringBuilder answer = new StringBuilder("[");
        for (String a : answersOptions) {
            answer.append("\"")
                    .append(a)
                    .append("\",");
        }
        answer.setLength(answer.length() - 1);
        answer.append("]");
        return answer.toString();
    }

    private void updateQuestion(String key, String val) {
        Bundle bundle = new Bundle();
        bundle.putString("key", key);
        bundle.putString("val", val);
        ((AddQuestionActivity) context).fragToMain("multiple-choice", bundle);
    }

    public void addAnswer() {
        if (answersOptions.size() >= 5) {
            return;
        }
        answersOptions.add("");
        if (answersOptions.size() >= 5) {
            addAnswerLayout.setVisibility(View.GONE);
        }
    }

    public void removeAnswer(int id) {
        answersOptions.remove(answersOptions.get(id));
        addAnswerLayout.setVisibility(View.VISIBLE);
    }

    public void checkAnswers() {
        for (ViewHolder holder : holders) {
            onFocusChange(holder.getTextInputAnswer(), holders.indexOf(holder), false, R.string.hint_answer);
        }
    }

    // Create new views (invoked by the layout manager)
    @NotNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        // Create a new view, which defines the UI of the list item
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.layout_answer_multiple_choice, viewGroup, false);
        ViewHolder holder = new ViewHolder(view);
        holders.add(holder);
        return holder;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {

        // Get element from your dataset at this position and replace the
        // contents of the view with that element
        TextInputLayout text = viewHolder.getTextInputAnswer();
        TextInputEditText edit = viewHolder.getEditTextAnswer();
        RadioButton radio = viewHolder.getRadioButtonAnswer();

        text.setHint(context.getString(R.string.hint_answer, position + 1));
        text.setEndIconOnClickListener(view -> removeAnswer(position));
        edit.setText(answersOptions.get(position));
        edit.setOnFocusChangeListener((v, focused)
                -> onFocusChange(text, position, focused, R.string.hint_answer));
        edit.addTextChangedListener(
                new TextChangedListener<TextInputEditText>(edit) {
                    @Override
                    public void onTextChanged(TextInputEditText target, Editable s) {
                        setAnswer(position, s);
                    }
                });
        radio.setChecked(answerId == position);
        radio.setOnClickListener((v) -> setAnswerId(position));
        // if (pos <= 2) {
        text.setEndIconVisible(false);
        // }
    }

    private void onFocusChange(TextInputLayout inputLayout, int position, boolean focused, @StringRes int stringId) {
        if (!focused) {
            String str = Objects.requireNonNull(inputLayout.getEditText()).getText().toString();
            if (str.isEmpty()) {
                inputLayout.setHint(String.format(context.getString(stringId), position));
                inputLayout.setBackground(
                        AppCompatResources.getDrawable(
                                context,
                                R.drawable.rounded_input_negative
                        )
                );
            } else {
                inputLayout.setHint("");
                inputLayout.setBackground(
                        AppCompatResources.getDrawable(
                                context,
                                R.drawable.rounded_input
                        )
                );
            }
        }
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return answersOptions.size();
    }
}