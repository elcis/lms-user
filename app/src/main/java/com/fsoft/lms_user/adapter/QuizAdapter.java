package com.fsoft.lms_user.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.fsoft.lms_user.R;
import com.fsoft.lms_user.model.Quiz;

import java.util.ArrayList;

public class QuizAdapter extends RecyclerView.Adapter<QuizAdapter.QuizViewHolder> {
    private ArrayList<Quiz> quizzesList;
    private OnItemCLickListener mListener;

    public interface OnItemCLickListener {
        void onItemClick(int position);
    }

    public void setOnItemClickListener(OnItemCLickListener listener) {
        mListener = listener;
    }

    //-------------------------------------------------------------
    public static class QuizViewHolder extends RecyclerView.ViewHolder {
        public ImageView ivQuizThumbnail;
        public TextView tvQuizName, tvNumberOfQuestions, tvQuizAuthor;

        //-------------------------------------------------------------
        public QuizViewHolder(@NonNull View itemView, OnItemCLickListener listener) {
            super(itemView);
            ivQuizThumbnail = itemView.findViewById(R.id.ivQuizThumbnail);
            tvQuizName = itemView.findViewById(R.id.tvQuizName);
            tvNumberOfQuestions = itemView.findViewById(R.id.tvNumberOfQuestions);
            tvQuizAuthor = itemView.findViewById(R.id.tvQuizAuthor);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            listener.onItemClick(position);
                        }
                    }
                }
            });
        }
    }

    @NonNull
    @Override
    public QuizViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_search_quiz_result, parent, false);
        QuizViewHolder quizViewHolder = new QuizViewHolder(view, mListener);

        return quizViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull QuizViewHolder holder, int position) {
        Quiz currentQuiz = quizzesList.get(position);

        if (currentQuiz.getAvatar() != null && !currentQuiz.getAvatar().equals("NONE")) {
            Glide.with(holder.ivQuizThumbnail.getContext())
                    .load(currentQuiz.getAvatar())
                    .centerCrop()
                    .into(holder.ivQuizThumbnail);

            holder.ivQuizThumbnail.setPadding(0, 0, 0, 0);
        } else {
            holder.ivQuizThumbnail.setImageResource(R.drawable.ic_picture);
            int padding = holder.ivQuizThumbnail.getContext().getResources().getDimensionPixelSize(R.dimen.image_placeholder_padding);
            holder.ivQuizThumbnail.setPadding(padding, padding, padding, padding);
        }

        String textNumberOfQuestions = currentQuiz.getQuestion().size() + " Qs";
        String textAuthor = "By: " + currentQuiz.getAuthor();

        holder.tvQuizName.setText(currentQuiz.getName());
        holder.tvNumberOfQuestions.setText(textNumberOfQuestions);
        holder.tvQuizAuthor.setText(textAuthor);
    }

    @Override
    public int getItemCount() {
        return quizzesList.size();
    }

    //-------------------------------------------------------------
    public QuizAdapter(ArrayList<Quiz> list) {
        this.quizzesList = list;
    }
}
