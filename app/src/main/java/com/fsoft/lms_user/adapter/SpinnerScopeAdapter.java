package com.fsoft.lms_user.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.fsoft.lms_user.R;
import com.fsoft.lms_user.model.ScopeItem;

import org.jetbrains.annotations.NotNull;
import org.w3c.dom.Text;

import java.util.ArrayList;

public class SpinnerScopeAdapter extends ArrayAdapter<ScopeItem> {
    public SpinnerScopeAdapter(Context context, ArrayList<ScopeItem> scopeList) {
        super(context, 0, scopeList);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return initView(position, convertView, parent);
    }

    @Override
    public View getDropDownView(int position, @Nullable @org.jetbrains.annotations.Nullable View convertView, @NonNull @NotNull ViewGroup parent) {
        return initView(position, convertView, parent);
    }

    private View initView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(
                    R.layout.item_scope_spinner, parent, false
            );
        }

        ImageView iconScope = convertView.findViewById(R.id.iconScope);
        TextView tvScope = convertView.findViewById(R.id.tvScope);

        ScopeItem currentItem = getItem(position);

        if (currentItem != null) {
            iconScope.setImageResource(currentItem.getScopeIcon());
            tvScope.setText(currentItem.getScopeName());
        }

        return convertView;
    }
}
