package com.fsoft.lms_user.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.fsoft.lms_user.R;
import com.fsoft.lms_user.model.QuestionPreview;

import java.util.ArrayList;

public class QuestionPreviewAdapter extends RecyclerView.Adapter<QuestionPreviewAdapter.QuestionPreviewViewHolder>{
    private ArrayList<QuestionPreview> questionPreviewsList;

    //-------------------------------------------------------------
    public static class QuestionPreviewViewHolder extends RecyclerView.ViewHolder {
        public ImageView ivQuestionPreviewThumbnail;
        public TextView tvQuestionPreviewType, tvQuestionPreviewContent;

        //-------------------------------------------------------------
        public QuestionPreviewViewHolder(@NonNull View itemView) {
            super(itemView);
            ivQuestionPreviewThumbnail = itemView.findViewById(R.id.ivQuestionThumbnail);
            tvQuestionPreviewType = itemView.findViewById(R.id.tvQuestionPreviewType);
            tvQuestionPreviewContent = itemView.findViewById(R.id.tvQuestionPreviewContent);
        }
    }

    //-------------------------------------------------------------
    @NonNull
    @Override
    public QuestionPreviewViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_question_preview, parent, false);
        QuestionPreviewViewHolder questionPreviewViewHolder = new QuestionPreviewViewHolder(view);

        return questionPreviewViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull QuestionPreviewViewHolder holder, int position) {
        QuestionPreview currentQuestionPreview = questionPreviewsList.get(position);

        if (currentQuestionPreview.getImageURL() != null && !currentQuestionPreview.getImageURL().equals("NONE")) {
            Glide.with(holder.ivQuestionPreviewThumbnail.getContext())
                    .load(currentQuestionPreview.getImageURL())
                    .centerCrop()
                    .into(holder.ivQuestionPreviewThumbnail);

            holder.ivQuestionPreviewThumbnail.setPadding(0,0,0,0);
        } else {
            holder.ivQuestionPreviewThumbnail.setImageResource(R.drawable.ic_picture);

            int padding = holder.ivQuestionPreviewThumbnail.getContext().getResources().getDimensionPixelSize(R.dimen.image_placeholder_padding);
            holder.ivQuestionPreviewThumbnail.setPadding(padding, padding, padding, padding);
        }


        holder.tvQuestionPreviewType.setText(currentQuestionPreview.getType());
        holder.tvQuestionPreviewContent.setText(currentQuestionPreview.getQuestion());
    }

    @Override
    public int getItemCount() {
        return questionPreviewsList.size();
    }
    //-------------------------------------------------------------
    public QuestionPreviewAdapter(ArrayList<QuestionPreview> list) {
        this.questionPreviewsList = list;
    }
}
