package com.fsoft.lms_user.model;

public class QuestionPreview {
    String _id;
    String type;
    String imageURL;
    String question;

    public QuestionPreview(String _id, String type, String imageURL, String question) {
        this._id = _id;
        this.type = type;
        this.imageURL = imageURL;
        this.question = question;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    @Override
    public String toString() {
        return "QuestionPreviewItem{" +
                "_id='" + _id + '\'' +
                ", type='" + type + '\'' +
                ", imageURL='" + imageURL + '\'' +
                ", question='" + question + '\'' +
                '}';
    }
}
