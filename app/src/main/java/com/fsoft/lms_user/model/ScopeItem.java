package com.fsoft.lms_user.model;

public class ScopeItem {
    private String scopeName;
    private int scopeIcon;

    public ScopeItem(String scopeName, int scopeIcon) {
        this.scopeName = scopeName;
        this.scopeIcon = scopeIcon;
    }

    public String getScopeName() {
        return scopeName;
    }

    public void setScopeName(String scopeName) {
        this.scopeName = scopeName;
    }

    public int getScopeIcon() {
        return scopeIcon;
    }

    public void setScopeIcon(int scopeIcon) {
        this.scopeIcon = scopeIcon;
    }
}
