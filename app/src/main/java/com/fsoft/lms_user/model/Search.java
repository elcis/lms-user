package com.fsoft.lms_user.model;

public class Search {
    String quizName;

    public Search(String quizName) {
        this.quizName = quizName;
    }

    public String getQuizName() {
        return quizName;
    }

    public void setQuizName(String quizName) {
        this.quizName = quizName;
    }

    @Override
    public String toString() {
        return "Search{" +
                "quizName='" + quizName + '\'' +
                '}';
    }
}
