package com.fsoft.lms_user.model;

import java.io.Serializable;
import java.util.List;

public class Question implements Serializable {
    String _id;
    String username;
    String category;
    String type;
    String time;
    String image;
    String question;
    List<String> answer;
    List<String> correct_answer;
    String description;

    public Question(String _id, String username, String category, String type, String time, String image, String question, List<String> answer, List<String> correct_answer, String description) {
        this._id = _id;
        this.username = username;
        this.category = category;
        this.type = type;
        this.time = time;
        this.image = image;
        this.question = question;
        this.answer = answer;
        this.correct_answer = correct_answer;
        this.description = description;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public List<String> getAnswer() {
        return answer;
    }

    public void setAnswer(List<String> answer) {
        this.answer = answer;
    }

    public List<String> getCorrect_answer() {
        return correct_answer;
    }

    public void setCorrect_answer(List<String> correct_answer) {
        this.correct_answer = correct_answer;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Question{" +
                "_id='" + _id + '\'' +
                ", username='" + username + '\'' +
                ", category='" + category + '\'' +
                ", type='" + type + '\'' +
                ", time='" + time + '\'' +
                ", image='" + image + '\'' +
                ", question='" + question + '\'' +
                ", answer=" + answer +
                ", correct_answer=" + correct_answer +
                ", description='" + description + '\'' +
                '}';
    }
}