package com.fsoft.lms_user.model;

import java.io.Serializable;
import java.util.List;

public class Quiz implements Serializable {
    String _id;
    String name;
    String category;
    String avatar;
    String privacy;
    String author;
    String time;
    List<String> question;

    public Quiz(String _id, String name, String category, String avatar, String privacy, String author, String time, List<String> question) {
        this._id = _id;
        this.name = name;
        this.category = category;
        this.avatar = avatar;
        this.privacy = privacy;
        this.author = author;
        this.time = time;
        this.question = question;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getPrivacy() {
        return privacy;
    }

    public void setPrivacy(String privacy) {
        this.privacy = privacy;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public List<String> getQuestion() {
        return question;
    }

    public void setQuestion(List<String> question) {
        this.question = question;
    }

    @Override
    public String toString() {
        return "Quiz{" +
                "_id='" + _id + '\'' +
                ", name='" + name + '\'' +
                ", category='" + category + '\'' +
                ", avatar='" + avatar + '\'' +
                ", privacy='" + privacy + '\'' +
                ", author='" + author + '\'' +
                ", time='" + time + '\'' +
                ", question=" + question +
                '}';
    }
}
