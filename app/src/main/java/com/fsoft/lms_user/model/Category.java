package com.fsoft.lms_user.model;

import java.util.List;

public class Category {
    String _id;
    String categoryName;
    String parentId;

    public Category(String _id, String categoryName, String parentId) {
        this._id = _id;
        this.categoryName = categoryName;
        this.parentId = parentId;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    @Override
    public String toString() {
        return "_id='" + _id + '\n' +
                ", categoryName=" + categoryName + '\n' +
                ", parentId=" + parentId + '\n' +
                "\n\n";
    }
}
